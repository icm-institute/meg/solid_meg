% figures 6 10 14 18 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','grad','hm','transform'});

p_neighb

root_jobsdir = fullfile(jobsdir,'figure6');mymkdir(root_jobsdir)
figdir = fullfile(figsdir,'Figure6');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))
load layout

%%

% number of resamples
nMC = 500;

% benchmark of subjects and trials
nsujs = 5:5:50;
nnsujs = numel(nsujs);
ntris = [2 20:20:200];    % keep rem(ntris,2) == 0
nntris = numel(ntris);

dipamp = 5;


mctxsm = p_create_meanctx('32',fs);

rng(123)

[sm_specs,initial] = p_figure06_10_14_18_source_specs(mctxsm);

%% t-test to cluster
if not(exist(fullfile(root_jobsdir,'common.mat'),'file'))
    allgrad = {fs.grad};
    save(fullfile(root_jobsdir,'common.mat'),'allgrad','channeighbstructmat','alldata');
end
all_cmd = {};
for ispec = 1:numel(sm_specs)
    for isuj = 1:size(fs,1)
        [fs] = p_figure06_10_14_18_source_comp(fs,isuj,sm_specs(ispec),initial);
    end
    fs_noctxsm = rmfield(fs,{'grad'});
    
    specif = [];
    specif.amp = dipamp;
    specif.nsujs = nsujs;
    specif.ntris = ntris;
    specif.nMC = nMC;
    specif.nperm = 2000;
    specif.clustthresh = .05;
    specif.finalpthresh = .05;
    specif.jobfilename = 'p_figure06_10_14_18_c_jobs';
    specif.jobsdir = fullfile(root_jobsdir ,[num2str(ispec,'%02d') '_' sm_specs(ispec).name]);
    specif.doperm = 0;
    
    
    scriptsdir = fullfile(specif.jobsdir,'scripts/');
    
    allMCs = p_create_MCs(fs,specif.nMC, specif.nsujs, specif.ntris);
    [allMCs.times] = rep2struct(1:50);
    
    cfg = [];
    cfg.jobfilename = specif.jobfilename;
    cfg.jobnickname = 'Post-it';
    cfg.scriptsdir = codedir;
    cfg.scriptsdir2exclude = {'*.csv','*.fig','*.png','mctxsm*.mat','*.gif','fieldtrip','db','megconnectome*'};
    cfg.jobsdir = specif.jobsdir;
    cfg.vars2save = struct('fs',fs_noctxsm,'specif',specif);
    cfg.var2slice = struct('MCs',allMCs);
    cfg.slicedim = 3;
    cfg.njobs = specif.nMC;
    cfg.sbatchcfg.timelimit = '04:00:00';
    cfg.sbatchcfg.mem = '4G';
    cfg.onlycommand = 0;
    cfg.doithere = 0;
    
    all_cmd{ispec} = p_send2cluster(cfg);
    disp(all_cmd{ispec});
end
fprintf('\n')
cellfun(@disp,all_cmd);

%% model responses
% clear coefs_*

disp(root_jobsdir)
for ispec = ispec:numel(sm_specs)
    % gather results
    [MCs] = simuHCP_gather_clusterjobs(fullfile(root_jobsdir, [num2str(ispec,'%02d') '_' sm_specs(ispec).name]));
    
    booboots = struct2table(MCs(:));
    booboots.sujs = cellfun(@numel,booboots.sujs);
    booboots.tris = cellfun(@(x) size(x,2),booboots.tris);
    booboots.name = repmat(sm_specs(ispec).name,size(booboots,1),1);
    booboots.amp = repmat(dipamp,size(booboots,1),1);
    booboots.ispec = repmat(ispec,size(booboots,1),1);
    if not(ismember('hperm',booboots.Properties.VariableNames))
        booboots.hperm = booboots.h;
    end
    
    % remove useless variables
    vs = booboots.Properties.VariableNames;
    v = vs(regexpcell(vs,{'tstat','df','sd','m','p','m','times'},'exact'));
    booboots(:,v) = [];
    
    posvar = sm_specs(ispec).posvar;
    orivar = sm_specs(ispec).momvar;
    dsens = mctxsm.dvar(seed);
    orisens = rad2deg(acos(mctxsm.radiality(seed)));
    switch sm_specs(ispec).name(1:5)
        case {'novar' 'posva' 'oriva'}
        case 'dsens'
            dsens = sm_specs(ispec).chand + initial.d;
        case 'orise'
            orisens = sm_specs(ispec).chanmom;
    end
    
    booboots.posvar = repmat(posvar,size(booboots,1),1);
    booboots.orivar = repmat(orivar,size(booboots,1),1);
    booboots.dsens = repmat(dsens,size(booboots,1),1);
    booboots.orisens = repmat(orisens,size(booboots,1),1);
    
    % write to csv
    writetable(booboots,fullfile(root_jobsdir,['fig5_' num2str(ispec,'%02d') '_' sm_specs(ispec).name '.csv']));
end

% rchunk compute_pred_perm
%
% library(plyr)
% library(tidyverse)
% library(emmeans)
%
% boots <- ddply(tibble(spec=list.files(pattern='fig5_.._.*.csv')),'spec',function(x){read.csv(x$spec)}) %>%
%   as_tibble()
%
% b <- boots %>%
%   group_by(sujs,tris,amp,name) %>%
%   summarize(h = sum(hh),n = n(),p=h/n)
%
% bperm <- boots %>%
%   group_by(sujs,tris,amp,name) %>%
%   summarize(hperm = sum(hperm),n = n(),pperm = hperm/n)
%
% specs <- boots %>%
%   group_by(name) %>% summarize(ispec = first(ispec))
%
% urboots <- boots
%
% for (thispec in specs$ispec) {
%   boots <- urboots %>% filter(ispec == thispec)
%   spec <- filter(specs,ispec==thispec)$name
%   m <- glm(h ~ log10(sujs)*log10(tris),data=boots,family='binomial')
%   mperm <- glm(hperm ~ log10(sujs)*log10(tris),data=boots,family='binomial')
%
%
%   em <- emmeans(m,~sujs*tris,at=list(sujs=unique(boots$sujs),tris=seq(min(boots$tris),max(boots$tris))))
%   emperm <- emmeans(mperm,~sujs*tris,at=list(sujs=unique(boots$sujs),tris=seq(min(boots$tris),max(boots$tris))))
%
%   emtoplot <- as_tibble(summary(em,type='response'))
%   emtoplotperm <- as_tibble(summary(emperm,type='response'))
%
%   write.csv(bperm %>% filter(name == spec),paste0('fig5_predperm_',sprintf('%02d',thispec),'_', spec,'.csv'))
%   write.csv(emtoplotperm,paste0('fig5_perfperm_',sprintf('%02d',thispec),'_', spec,'.csv'))
% }
%

runr('simuHCP_paper_figure5_time.m','name','compute_pred_perm','dir',root_jobsdir);

%% power plot

props = {'dsens','orisens','posvar','orivar'};
for iprop = 1:numel(props)
    %%
    tmp_sm_specs = sm_specs(regexpcell({sm_specs.name},props{iprop}));
    fig(15,3838+iprop);
    clf
    for ispec = 1:numel(tmp_sm_specs)
        subplot(1,numel(tmp_sm_specs),ispec)
        set(gca,'FontSize',14);
        colormap(gca,varycolor(256,'Moreland_Black_Body'))
        hold on
        f = flister(['fig5_predperm_.._' tmp_sm_specs(ispec).name '.csv'],'dir',root_jobsdir,'recurse',0);
        pred=readtable(f.name);
        toplot = reshape(pred.pperm,nntris,nnsujs);
        imagesc(nsujs,ntris,toplot,[0 1])
        %     if numel(toplot(:)==1) > 35
        levs = [.1 .5 .8];
        %     else
        %         levs = [.1:.1:.8];
        %     end
        [xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));
        %     imagesc(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,[0 1])
        nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
        contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
        set(gca,'layer','top')
        box on
        axis xy
        
        xticks(nsujs)
        xticklabels(nsujs);
        set(gca,'XTickLabelRotation',90)
        yticks(ntris)
        yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
        if ispec == 1
            
            xlabel('Number of subjects')
            ylabel('Number of trials')
        end
        set(gca,'tag','power_contour')
        
        if ispec == numel(tmp_sm_specs)
            h = colorbar('Position',[0.9291    0.1841    0.0152    0.7292]);
            h.Label.String = 'Power';
            h.Label.FontSize =16;
        end
        
    end
    %%
    set(gcf,'paperpositionmode','auto')
    drawnow
    
    print('-dpng','-r300',fullfile(figdir,['PowerPlot_' props{iprop} '.png']))
    
end





%%
% coefs_simple = readtable('coefs_simple.csv');
% coefs_cluster = readtable('coefs_cluster.csv');
%
% figure(4848);clf;
% for i = 2:5
%     subplot(2,2,i-1)
%     plot([coefs_simple{:,i} coefs_cluster{:,i}]')
%     title(coefs_simple.Properties.VariableNames{i})
% end
% figure(3838);clf;
% hold on
% X = doconds({nsujs,ntris});
% X = [X{1}(:) X{2}(:) X{1}(:).*X{2}(:)];
% for i = 1%:size(coefs_simple,1)
%     p = glmval(coefs_simple{i,2:5}',X,'logit');
%     p = reshape(p,[nnsujs,nntris])';
%     plot(ntris,p,'+-')
% end
% for i = 1%:size(coefs_simple,1)
%     p = glmval(coefs_cluster{i,2:5}',X,'logit');
%     p = reshape(p,[nnsujs,nntris])';
%     plot(ntris,p,'o-')
% end
%% Now create plots for each variability source

for ivar = 1:numel(specdef)
    
    sm_spec_tmp = sm_specs(regexpcell({sm_specs.name},specdef(ivar).var));
    
    
    
end


%% Now final figures showing:
% head model
% topography
% histogram of spatial and momentum distributions
% power plot

load(fullfile(root_jobsdir,'common.mat'),'allgrad')
%
for ispec = 1%:numel(sm_specs)
    jobsdir = fullfile(root_jobsdir ,[num2str(ispec,'%02d') '_' sm_specs(ispec).name]);
    jobscommon = load(fullfile(jobsdir,'clusterjob_common.mat'));
    [jobscommon.fs.grad] = rep2struct(allgrad);
    
    
    tmpgrad = fs(1).grad;
    [sel,meglabels] = chnb('meg',tmpgrad.label);
    tmpgrad.label = tmpgrad.label(sel);
    tmpgrad.chanori = tmpgrad.chanori(sel,:);
    tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
    tmpgrad.chantype = tmpgrad.chantype(sel);
    tmpgrad.chanunit = tmpgrad.chanunit(sel);
    tmpgrad.tra = tmpgrad.tra(sel,:);
    %     scatter3(tmpgrad.chanpos(chnb(urclosest,tmpgrad.label),1),tmpgrad.chanpos(chnb(urclosest,tmpgrad.label),2),tmpgrad.chanpos(chnb(urclosest,tmpgrad.label),3),'.r')
    %     ft_plot_vol(jobscommon.fs(1).hm,'edgecolor','none','facecolor','skin','facealpha',.5)
    %     lighting gouraud
    %     material dull
    %     camlight
    %     title(sm_specs(ispec).name);
    %%
    fig(22,4777+ispec);clf
    % whole brain view
    %     annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.97 0.0249 0.0271],'String','a');
    subplot(2,3,1)
    ft_plot_vol(jobscommon.fs(1).hm,'edgecolor','none','facecolor','skin','facealpha',.5)
    hold on
    ft_plot_sens(tmpgrad)
    for i = 1:size(jobscommon.fs,1)
        quiver3(jobscommon.fs(i).sm.pos(1),jobscommon.fs(i).sm.pos(2),jobscommon.fs(i).sm.pos(3),jobscommon.fs(i).sm.mom(1),jobscommon.fs(i).sm.mom(2),jobscommon.fs(i).sm.mom(3),.1,'linewidth',2)
    end
    view(3)
    lighting gouraud
    material dull
    camlight
    
    % histogram of position variability
    subplot(2,3,2)
    %     annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.70 0.0249 0.0271],'String','b');
    allpos = cellfun(@(x)x.pos,{jobscommon.fs(:,1).sm},'uniformoutput',0);
    allpos = vertcat(allpos{:});
    set(gca,'Color','none', 'ColorOrder',varycolor(size(allpos,2)));
    set(gca,'fontsize',14)
    hold on
    mpos = mean(allpos,1);
    allpos = allpos - mpos;
    [c,ce] = hist(allpos*1000,linspace(-30, 30,20));
    bar(ce,c);
    xlabel('distance (mm)')
    ylabel('count')
    legend({'x','y','z'},'box','off','position',[0.5714    0.8275    0.0565    0.1020])
    xlim(maxabs(ce,'+-'))
    box off
    
    subplot(2,3,3)
    % histograms of orientation variability
    %     annotation('textbox','fontsize',20,'linestyle','none','position',[0.55 0.70 0.0249 0.0271],'String','c');
    allmom = cellfun(@(x)x.mom,{jobscommon.fs(:,1).sm},'uniformoutput',0);
    allmom = horzcat(allmom{:})';
    norm(mean(allmom,1))
    [allmomsph(:,1),allmomsph(:,2)] = cart2sph(allmom(:,1),allmom(:,2),allmom(:,3));
    mmom = circ_mean(allmomsph,[],1);
    allmomsph = allmomsph- mmom;
    % compute radiality to center histogram
    for i_f = 1:size(jobscommon.fs,1)
        % find closest sensor
        [ichan] = dsearchn(jobscommon.fs(i_f).grad.chanpos,jobscommon.fs(i_f).sm.pos);
        % compute orientation relative to that sensor
        % keep value
        o(i_f) = dot(jobscommon.fs(i_f).sm.mom , jobscommon.fs(i_f).grad.chanori(ichan,:));
    end
    radiality = mean(o);
    rd = real(acos(radiality));
    allmomsph = allmomsph + rd;
    for i = 1:size(allmomsph,2)
        polarhistogram(allmomsph(:,i),linspace(-pi,pi,40));
        hold on
    end
    tmp = get(gca,'RLim');
    h = polarplot(gca,[rd rd],[0 norm(mean(allmom,1)) * tmp(2)],'k','linestyle','-','linewidth',2);
    set(gca,'ThetaDir','clockwise','ThetaZeroLocation','top','RAxisLocation',330,'FontSize',16)
    Tt = [0:pi/6:2*pi rd] / pi * 180;
    [Tt,idx] = unique(Tt);
    Ttl = {'Radial' '' '' '' '' '' '' '' '' '' '' '' '' [num2str(round(rd/pi*180)) '°']};Ttl = Ttl(idx);
    set(gca,'ThetaTick',Tt,'ThetaTickLabel',Ttl)
    set(gca,'Rtick',linspace(tmp(1),tmp(2),5),'Rticklabel','')
    text(gca,3*pi/2,diff(tmp)/5,'Tangential','FontSize',get(gca,'FontSize'),'HorizontalAlignment','right','VerticalAlignment','middle')
    legend({'azimuth','elevation'},'box','off','position',[  0.6380    0.8531    0.1296    0.0793])
    box off
    
    % topo
    subplot(2,3,4);cla
    %     annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.35 0.0249 0.0271],'String','d');
    clear lftopo
    for isuj = 1%:size(jobscommon.fs,1)
        lftopo(:,:,isuj) = jobscommon.fs(isuj).lf * jobscommon.fs(isuj).sm.mom * dipamp;
    end
    lftopo = mean(lftopo,3)*1e15;
    [m,maxsensor] = maxabs(lftopo);
    tmp = [];
    tmp.avg = lftopo;
    tmp.time = 1;
    [~, tmp.label] = chnb('meg',jobscommon.fs(isuj).grad.label);
    tmp.grad = jobscommon.fs(1).grad;
    tmp.dimord = 'chan_time';
    % projection to sensors
    cfg = [];
    cfg.layout = layout;
    cfg.zlim = [min(-m,-30) max(m,30)];%'maxabs'; %[-3.76 3.76]*1e-13;
    cfg.interactive = 'no';
    cfg.comment = 'no';
    cfg.gridscale = 200;
    ft_topoplotER(cfg,tmp);
    %     set(gca,'position',[0.12   0.03    0.33 .33]);
    colormap(varycolor(256,'viridis'));
    h = colorbar('fontsize',22,'position',[ 0.1184    0.1151    0.0200    0.3000]);
    ylabel(h,'Amplitude (fT)');
    
    subplot(2,3,[5 6])
    set(gca,'position',[0.4108    0.1100    0.4141    0.3412]);
    % power plot
    %     annotation('textbox','fontsize',20,'linestyle','none','position',[0.47 0.35 0.0249 0.0271],'String','e');
    set(axes,'FontSize',14,'position',[0.5200    0.0619    0.3700    0.2700])
    hold on
    pred=readtable(fullfile(root_jobsdir,['fig3_predperm_' num2str(i_ar,'%02d') '_' all_areas.parcels{i_ar} '.csv']));
    toplot = reshape(pred.pperm,nntris,nnsujs);
    imagesc(nsujs,ntris,toplot,[0 1])
    if numel(toplot(:)==1) > 35
        levs = [.1 .5 .9];
    else
        levs = [.1:.1:.9];
    end
    [xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));
    nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
    contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
    axis xy
    xticks(nsujs)
    yticks(ntris)
    xticklabels(nsujs);
    yticklabels(ntris);
    xlabel('Number of trials')
    ylabel('Number of subjects')
    h = colorbar('position',[0.9116    0.0619    0.0196    0.27]);
    h.Label.String = 'Power';
    h.Label.FontSize =14;
    %     hold on
    %     cmap = varycolor(numel(nsujs),'ggplot');
    %     set(gca,'colororder',cmap);
    %     pred=readtable(fullfile(root_jobsdir,['fig5_predperm_' num2str(ispec,'%02d') '_' sm_specs(ispec).name '.csv']));
    %     perf=readtable(fullfile(root_jobsdir,['fig5_perfperm_' num2str(ispec,'%02d') '_' sm_specs(ispec).name '.csv']));
    %     clear h
    %     for i = 1:numel(nsujs)
    %         idx = pred.sujs==nsujs(i);
    % %         scatter(pred.tris(idx),pred.pperm(idx),[],cmap(i,:),'filled');
    % %         idx2 = perf.sujs == nsujs(i);
    % %         x = perf(idx2,:).tris;
    % %         y = perf(idx2,:).prob;
    % %         h(i) = plot(x,y,'Color',colors('grey90'),'linewidth',1);
    % %         l = perf(idx2,:).asymp_LCL;
    % %         u = perf(idx2,:).asymp_UCL;
    % %         shadebetween(x,u,l,cmap(i,:),'none',.2)
    %
    %         h(i) = plot(pred.tris(idx),pred.pperm(idx),'-','color',cmap(i,:),'MarkerSize',20,'Marker','.','linewidth',1.5);
    %     end
    %     ylim([0 1])
    %     xlim([0 max(ntris)])
    %     ylabel('Power')
    %     xlabel('Trials')
    %     xticks(ntris)
    %     xticklabel(num2str(cellstr2num(get(gca,'xticklabel'))/2))
    %     lgd = legend(h,num2cellstr(nsujs),'position',[0.8435    0.0995    0.0791    0.3700],'Box','off');
    %     lgd.FontSize = 14;
    %     lgd.Title.String = {'number' 'of' 'subjects'};
    %     lgd.Title.FontSize = 14;
    %     lgd.Title.FontWeight = 'normal';
    %     set(gcf,'name',sm_specs(ispec).name);
    drawnow
    
    figure(4777+ispec);
    %
    %     set(gcf,'paperpositionmode','auto')
    %     print('-dpng','-r300',fullfile(root_jobsdir,[num2str(ispec,'%02d') '_' sm_specs(ispec).name '.png']))
    %     export_fig(fullfile('/home/maximilien.chaumon/owncloud/Lab/Projects/SOLID_MEEG/docs/Paper01',[num2str(ispec,'%02d') '_' strrep(all_areas.parcels{ispec},'L_L_','') '.png']),'-r300','-nocrop')
    %     close
end




function moveright(h,d)
h.Position = h.Position + [d 0 0 0];
end
