function [sm_specs,initial] = p_figure06_10_14_18_source_specs(mctxsm)


%% define source space
% defining initial source

p_all_areas
% #9 in the all_areas list
% urXXX variables point to this initial source below
i_ar = 9;
seed = all_areas.seeds(i_ar);
initial = [];
initial.pos = mctxsm.pos(seed,:);
initial.mom = [mctxsm.mnrm(seed,:)]; % average orientation across subjs (see mctxsm)
initial.mom = initial.mom./norm(initial.mom); % normalize
initial.closest = mctxsm.closestchan(seed);
initial.d = mctxsm.dvar(seed,:)/100;

specdef = [];
% cross-subj position variability
specdef(end+1).var = 'posvar';
specdef(end).par = 'posvar';
specdef(end).parval = linspace(0,10,5)*1e-3;
% cross-subj orientation variability
specdef(end+1).var = 'orivar';
specdef(end).par = 'momvar';
specdef(end).parval = linspace(0,180,5);
% distance to closest sensor
specdef(end+1).var = 'dsens';
specdef(end).par = 'chand';
specdef(end).parval = linspace(.02,.1,5);
% orientation wrt closest sensor
specdef(end+1).var = 'orisens';
specdef(end).par = 'chanmom';
specdef(end).parval = linspace(0,90,5);

sm_specs = [];
for idef = 1:numel(specdef)
    % for each definition in specdef
    for ispec = 1:numel(specdef(idef).parval)
        % copy a template specification with params of the initial source
        sm_specs(end+1).name = [specdef(idef).var num2str(specdef(idef).parval(ispec),'_%0.3g')];
        sm_specs(end).pos = [initial.pos];         % origin x y z
        sm_specs(end).posvar = [0];          % variability x y z
        sm_specs(end).mom = [initial.mom];         % momentum x y z
        sm_specs(end).momvar = [0];          % variability az el
        sm_specs(end).ampvar = 0;            % we never manipulate this
        sm_specs(end).chand = NaN;           % distance to sensor
        sm_specs(end).chanmom = NaN;         % wrt closest channel. az el
        % adapt only the necessary field
        sm_specs(end).(specdef(idef).par) = specdef(idef).parval(ispec);
    end
end

%% done with definition

if exist('fs','var')
    
    % if we measure chan position by distance to closest sensor
    if not(isnan(sm_specs.chand))
        % take position of closest sensor, switch to spherical coords,
        % add chand to radius, switch back to cartesian.
        tmp = fs(isuj).grad.chanpos(chnb(initial.closest,fs(isuj).grad.label),:);
        [tmp(1),tmp(2),tmp(3)] = cart2sph(tmp(1),tmp(2),tmp(3));
        tmp(3) = tmp(3) - sm_specs.chand;
        [tmp(1),tmp(2),tmp(3)] = sph2cart(tmp(1),tmp(2),tmp(3));
        fs(isuj).sm.pos = tmp;
    else
        % otherwise we just start with prespecified position.
        fs(isuj).sm.pos = sm_specs.pos;
    end
    % if we measure chan orientation wrt closest sensor
    if not(all(isnan(sm_specs.chanmom)))
        % start with orientation of closest sensor
        % find closest sensor:
        [ichan(isuj)] = dsearchn(fs(isuj).grad.chanpos,fs(isuj).sm.pos);
        [~, ichanname(isuj)] = chnb(ichan(isuj),fs(isuj).grad.label);
        % its orientation
        tmp = fs(isuj).grad.chanori(ichan(isuj),:);
        % rotate phi radians around N axis
        phi = deg2rad(sm_specs.chanmom(1));
        U = [tmp]';
        N = [0 1 0]';
        v = cos(phi) * U +(1-cos(phi))*dot(U,N)*N + sin(phi) * cross(N,U);
        % save mom
        fs(isuj).sm.mom = v;
    else
        % otherwise just take prespecified
        fs(isuj).sm.mom = sm_specs.mom';
    end
    
    % sampling positions normally in 3d
    tmp = randn(1,3) .* sm_specs.posvar;
    fs(isuj).sm.pos = fs(isuj).sm.pos + tmp;
    
    % sampling orientations normally
    % switch to spherical coords
    [tmp(1),tmp(2),tmp(3)] = cart2sph(fs(isuj).sm.mom(1),fs(isuj).sm.mom(2),fs(isuj).sm.mom(3));
    % add random orientation to azimuth & elevation
    tmp(1:2) = tmp(1:2) + randn(1,2) .* deg2rad(sm_specs.momvar/2);
    
    % add random amplitude change
    tmp(3) = tmp(3) + (rand .* 2 -1) .* sm_specs.ampvar;
    [fs(isuj).sm.mom(1),fs(isuj).sm.mom(2),fs(isuj).sm.mom(3)]  = sph2cart(tmp(1),tmp(2),tmp(3));
    
    % compute leadfield for this source
    fs(isuj).lf = ft_compute_leadfield(fs(isuj).sm.pos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
    fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
    fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
else 
    fs = [];
end