#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=06:00:00
#SBATCH --mem=4G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --nodes=1
#SBATCH --output=precompute_lf_job_%j.out 
#SBATCH --error=precompute_lf_job_%j.err
#SBATCH --job-name=burn_one_down
#SBATCH --array=1-50
 
module load MATLAB
matlab -nodesktop -nojvm -r "cd /network/lustre/iss01/cenir/analyse/meeg/00_max/SOLID_MEEG/replicate/simuHCP;p_figure02_precompute_lf_ctxsm_superhighres;quit"
