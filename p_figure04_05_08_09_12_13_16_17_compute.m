% figures 3 7 11 15 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','atlas32','grad','hm','transform'});

fs = renamefields(fs,'atlas32','ctxsm');
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end

p_neighb

root_jobsdir = fullfile(jobsdir,'figure4');mymkdir(root_jobsdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

mctxsm = p_create_meanctx('32',fs);
%% Simulation parameters
% number of resamples
nMC = 500;

% benchmark of subjects and trials
nsujs = 5:5:50;
nnsujs = numel(nsujs);
ntris = [2 20:20:200];    % keep rem(ntris,2) == 0
nntris = numel(ntris);
ntimepoints = 50;

dipamp = 10;

% all areas coords, vertices etc.
p_all_areas

%% t-test to cluster
mymkdir(root_jobsdir)
if not(exist(fullfile(root_jobsdir,'common.mat'),'file'))
    allctxsm = {fs.ctxsm};
    allgrad = {fs.grad};
    save(fullfile(root_jobsdir,'common.mat'),'allctxsm','allgrad','channeighbstructmat','alldata');
end
all_cmd = {};
for i_ar = 1:numel(all_areas.parcels)
    fprintf('%d\t%s\n',i_ar, all_areas.parcels{i_ar})
    % now t-testing
    fprintf('computing lead fields...')
    for isuj = 1:size(fs,1)
        %
        
        list = all_areas.seeds(i_ar);
        
        fs(isuj).dippos = mean(fs(isuj).ctxsm.pos(list,:),1);
        fs(isuj).dipmom = mean(fs(isuj).ctxsm.nrm(list,:),1)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
        
        fs(isuj).lf = ft_compute_leadfield(fs(isuj).dippos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
        fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
        fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
        if 0
            %%
            tmp = [];
            tmp.avg = fs(isuj).lf * fs(isuj).dipmom;
            tmp.time = 1;
            tmp.label = fs(isuj).label(chnb('meg',fs(isuj).label));
            tmp.grad = fs(isuj).grad;
            tmp.dimord = 'chan_time';

            figure(6868);clf;
            cfg = [];
            cfg.layout = '4D248.lay';
            cfg.zlim = 'maxabs';
            ft_topoplotER(cfg,tmp);
            drawnow
        end
    end
    fprintf('Done\n')
    fs_noctxsm = rmfield(fs,{'ctxsm','grad'});
    
    specif = [];
    specif.areas.parcels = all_areas.parcels{i_ar};
    specif.areas.coords = all_areas.coords(i_ar,:);
    specif.amp = dipamp;
    specif.nsujs = nsujs;
    specif.ntris = ntris;
    specif.nMC = nMC;
    specif.nperm = 1000;
    specif.clustthresh = .05;
    specif.finalpthresh = .05;
    specif.jobfilename = 'p_figure04_jobs';
    specif.jobsdir = fullfile(root_jobsdir ,[num2str(i_ar,'%02d') '_' all_areas.parcels{i_ar}]);
    specif.doperm = 1;
    
    
    scriptsdir = fullfile(specif.jobsdir,'scripts/');
    
    allMCs = p_create_MCs(fs,specif.nMC, specif.nsujs, specif.ntris);
    [allMCs.times] = rep2struct(1:ntimepoints);
    
    cfg = [];
    cfg.jobfilename = specif.jobfilename;
    cfg.jobnickname = 'dong_time_flex';
    cfg.scriptsdir = codedir;
    cfg.scriptsdir2exclude = {'*.csv','*.fig','*.png','mctxsm*.mat','*.gif','fieldtrip','db','megconnectome*'};
    cfg.jobsdir = specif.jobsdir;
    cfg.vars2save = struct('fs',fs_noctxsm,'specif',specif);
    cfg.var2slice = struct('MCs',allMCs);
    cfg.slicedim = 3;
    cfg.njobs = specif.nMC;
    cfg.sbatchcfg.timelimit = '04:00:00';
    cfg.sbatchcfg.mem = '4G';
    cfg.onlycommand = 0;
    cfg.doithere = 0;
    
    all_cmd{end+1} = p_send2cluster(cfg);
    disp(all_cmd{end});
end
fprintf('\n')
cellfun(@disp,all_cmd);

