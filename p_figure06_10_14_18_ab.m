% figures 6 10 14 18 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','grad','hm','transform'});

p_neighb

figdir = fullfile(figsdir,'Figure6');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

load layout

%% 
dipamp = 5;


mctxsm = p_create_meanctx('32',fs);

rng(123)

[sm_specs,initial] = p_figure06_10_14_18_source_specs(mctxsm);


%% illustrate all properties

props = {'dsens','orisens','posvar', 'orivar'};%
for iprop = 1:numel(props)
    %%
    % create two figures 
    fig(25,iprop+2455);clf reset
    fig(25,iprop+155);clf reset
    % pick the right specs
    tmp_sm_specs = sm_specs(regexpcell({sm_specs.name},props{iprop}));
    for ispec = 1:numel(tmp_sm_specs)
        figure(iprop+2455);
        ichan = [];ichanname = {};
        subplot(1,numel(tmp_sm_specs),ispec)
        xlim([-1 1])
        ylim([-1 1])
        zlim([-1 1])
        view(3)
        
        hold on
        clear tmp
        % for all subjects
        for isuj = 1:size(fs,1)
            [fs] = p_figure06_10_14_18_source_comp(fs,isuj,tmp_sm_specs(ispec),initial);
            % plot leadfield
            quiver3(fs(isuj).sm.pos(1),fs(isuj).sm.pos(2),fs(isuj).sm.pos(3),fs(isuj).sm.mom(1),fs(isuj).sm.mom(2),fs(isuj).sm.mom(3),.1,'linewidth',2)
        end
        
        % plot grad of 1st subject
        tmpgrad = fs(1).grad;
        [sel,meglabels] = chnb('meg',tmpgrad.label);
        tmpgrad.label = tmpgrad.label(sel);
        tmpgrad.chanori = tmpgrad.chanori(sel,:);
        tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
        tmpgrad.chantype = tmpgrad.chantype(sel);
        tmpgrad.chanunit = tmpgrad.chanunit(sel);
        tmpgrad.tra = tmpgrad.tra(sel,:);
        ft_plot_sens(tmpgrad)
        % highlight all closest channels in red
        % (actually not visible in figure)
        ichan = chnb(ichanname,tmpgrad.label);
        scatter3(tmpgrad.chanpos(ichan,1),tmpgrad.chanpos(ichan,2),tmpgrad.chanpos(ichan,3),'.r')
        % add skin surface of head model
        ft_plot_headmodel(fs(isuj).hm,'edgecolor','none','facecolor','skin','facealpha',.5)
        lighting gouraud
        material dull
        camlight
        title(tmp_sm_specs(ispec).name,'interpreter','none');
        drawnow
        
        % now plot projection to sensors
        figure(iprop+155);
        subplot(1,numel(tmp_sm_specs),ispec)
        clear lftopo
        % compute for all subjects
        for isuj = 1:size(fs,1)
            lftopo(:,:,isuj) = fs(isuj).lf * fs(isuj).sm.mom * dipamp;
        end
        % mean in fT
        lftopo = mean(lftopo,3)*1e15;
        [m,maxsensor] = maxabs(lftopo);
        % create data structure to plot
        tmp = [];
        tmp.avg = lftopo;
        tmp.time = 1;
        tmp.label = fs(1).label;
        tmp.grad = fs(1).grad;
        tmp.dimord = 'chan_time';
        cfg = [];
        cfg.layout = layout;
        cfg.zlim = [-50 50];
        cfg.interactive = 'no';
        cfg.comment = 'no';
        cfg.gridscale = 200;
        ft_topoplotER(cfg,tmp);
        
        colormap(gca,varycolor(256,{'blue','white','red'}));
        if ispec == numel(tmp_sm_specs)
            h = colorbar('position',[0.9307    0.2870    0.0156    0.4377],'fontsize',16);
            h.Label.String = 'Amplitude (fT)';
            h.Label.FontSize =16;
        end
        drawnow
    end
    figure(iprop+2455);
    set(gcf,'paperpositionmode','auto')
    drawnow
    %%
    print('-dpng','-r300',fullfile(figdir,['illustrate_' props{iprop} '.png']))
    
    figure(iprop+155);
    set(gcf,'paperpositionmode','auto')
    drawnow
    
    print('-dpng','-r300',fullfile(figdir,['topoillustrate_' props{iprop} '.png']))
end
