function cb = p_plotctx(mctx,vtx,cl,col_middle,col_bcg)

% addpath(cdhome('general/plot/panel'));
clf;
% p = panel();
% p.margin = 0;
% p.pack('h',{.18 .18 .18 .18 .18 .1});

if not(exist('cl','var'))
    cl = [0,1];
end
if not(exist('col_middle','var'))
    col_middle = 1;
end
if not(exist('col_bcg','var'))
    col_bcg = [1 1 1];
end
% set(gcf,'color',col_bcg)
% annotation(gcf,'rectangle',...
%     [0.0634142661179698 0.182291666666667 0.76374622770919 0.638020833333333],...
%     'FaceColor',col_bcg,'EdgeColor','none');

try
    toremove = regexpcell(mctx.parcellationlabel(mctx.parcellation),'\?');
    vtx(toremove) = NaN;
    tmp = false(size(mctx.parcellation));
    tmp(toremove) = true;
    tmpvtx = ones(size(vtx)) .* col_middle;
%     tmpvtx(~tmp) = NaN;
    tmpvtx = repmat(tmpvtx,1,3);
end
cb = @test_atlas_cb;

colormap(varycolor(256,'viridis'));
p(1) = axes('units','pixels','position',[1.0000    1.0000  262.4400  384.0000]);
% p(1).select();
% p(1).position = p(1).position +[.1 0 0 0];
set(gcf,'UserData',{mctx vtx});
if not(all(isnan(vtx)))
    ft_plot_mesh(mctx,'vertexcolor',tmpvtx);
    h = ft_plot_mesh(mctx,'vertexcolor',vtx);
else
    ft_plot_mesh(mctx);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgb[0 1]a(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','top')
h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(max(mctx.pos(:,1)),0,'A','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
%text(0,min(mctx.pos(:,2)),'R','HorizontalAlignment','left','VerticalAlignment','middle','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(-90,90)
camlight head
% rotate3d on

p(2) = axes('units','pixels','Position',[236.4400   88.0000  262.4400  384.0000]);
if not(all(isnan(vtx)))
    ft_plot_mesh(mctx,'vertexcolor',tmpvtx);
    ft_plot_mesh(mctx,'vertexcolor',vtx);
else
    ft_plot_mesh(mctx);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgba(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','left')
h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(max(mctx.pos(:,1)),0,.02,'A','HorizontalAlignment','right','VerticalAlignment','middle','fontsize',18,'tag','ori')
%text(0,0,max(mctx.pos(:,3)),'S','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(180,0)
camlight head
% rotate3d on

p(3) = axes('units','pixels','Position',[235.8800  -93.0000  262.4400  384.0000]);
[m] = ft_mesh_hemisphere(mctx,1);
if not(all(isnan(vtx)))
    ft_plot_mesh(m,'vertexcolor',tmpvtx);
    ft_plot_mesh(m,'vertexcolor',vtx);
else
    ft_plot_mesh(m);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgba(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','med')
h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(max(mctx.pos(:,1)),0,.02,'A','HorizontalAlignment','right','VerticalAlignment','middle','fontsize',18,'tag','ori')
%text(0,0,max(mctx.pos(:,3)),'S','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(0,0)
camlight head
% rotate3d on

% p(4).pack('v',2);
% p(4,1).select();
p(4) = axes('units','pixels','Position',[455.3200  188.0000  262.4400  192.0000]);
if not(all(isnan(vtx)))
    ft_plot_mesh(mctx,'vertexcolor',tmpvtx);
    ft_plot_mesh(mctx,'vertexcolor',vtx);
else
    ft_plot_mesh(mctx);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgba(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','back');
h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(0,min(mctx.pos(:,2)),.02,'R','HorizontalAlignment','left','VerticalAlignment','bottom','fontsize',18,'tag','ori')
%text(0,0,max(mctx.pos(:,3)),'S','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(-90,0)
camlight head
% rotate3d on

% p(4,2).select();
p(5) = axes('units','pixels','Position',[456.3200   1  262.4400  192.0000]);
if not(all(isnan(vtx)))
    ft_plot_mesh(mctx,'vertexcolor',tmpvtx);
    ft_plot_mesh(mctx,'vertexcolor',vtx);
else
    ft_plot_mesh(mctx);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgba(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','back');
h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(0,min(mctx.pos(:,2)),.02,'R','HorizontalAlignment','left','VerticalAlignment','bottom','fontsize',18,'tag','ori')
%text(0,0,max(mctx.pos(:,3)),'S','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(90,0)
camlight head
% rotate3d on

% p(5).select();
p(6) = axes('units','pixels','Position',[654.7600    4.0000  262.4400  384.0000]);
if not(all(isnan(vtx)))
    ft_plot_mesh(mctx,'vertexcolor',tmpvtx);
    ft_plot_mesh(mctx,'vertexcolor',vtx);
else
    ft_plot_mesh(mctx);
end
clim(cl)
% ft_plot_mesh(mctx,'vertexcolor',mctx.rgba(mctx.parcellation,:));
hold on
% scatter3(coords_close_to(1),coords_close_to(2),coords_close_to(3),1000,'*r');
set(gca,'tag','bottom');
set(gca,'position',get(gca,'position')-[.03 0 0 0])

h = datacursormode(gcf);
% datacursormode on
set(h,'updatefcn',cb);
%text(min(mctx.pos(:,1)),0,'P','HorizontalAlignment','center','VerticalAlignment','bottom','fontsize',18,'tag','ori')
%text(0,min(mctx.pos(:,2)),'R','HorizontalAlignment','left','VerticalAlignment','middle','fontsize',18,'tag','ori')
lighting gouraud
material dull
view(90,-90)
camlight head
% rotate3d on

% % squeeze axes together
% moveright(p(1),.1)
% moveright(p(2),.08)
% moveright(p(3),.07)
% moveright(p(4,1),.04)
% % moveup(p(4,1),.2)
% moveright(p(4,2),.04)
% % moveup(p(4,2),-.2)
% moveright(p(5),0.03)

% function moveright(p,d)
% h = p.axis;
% h.Position = h.Position + [d 0 0 0];
% 
% function moveup(p,d)
% h = p.axis;
% h.Position = h.Position + [0 d 0 0];

function output_txt = test_atlas_cb(obj,event_obj,dodel)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

if not(exist('dodel','var'))
    if isobject(obj)
        dodel = 1;
    else
        dodel = 0;
    end
end
n = get(findobj(gca,'-regexp','tag','dip\d+'),'tag');
if isempty(n)
    n = 1;
else
    n = max(cellfun(@(x)str2num(x(4:end)),unique(n))) + 1;
end

tmp = get(gcf,'userdata');
ctxsm = tmp{1};vtx = tmp{2};
if not(isfield(ctxsm,'areas'))
    ctxsm.areas.parcels = {};
    ctxsm.areas.coords = [];
end
try
    alt = 0;
    set(obj,'HandleVisibility','on');
%     if isempty(get(obj,'tag'))
%         n = numel(findobj(gcf,'type','hggroup'));
%         set(obj,'tag',['datatip' num2str(n)]);
%     end
%     n = get(obj,'tag');n = str2num(n(8));
    pos = get(event_obj,'Position');
    output_txt = {['X: ',num2str(pos(1),4)],...
        ['Y: ',num2str(pos(2),4)]};
    % If there is a Z-coordinate in the position, display it as well
    if length(pos) > 2
        output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
    end
catch
    output_txt = {''};
    idx = event_obj;
    pos = ctxsm.pos(idx,:);
end

try
    output_txt(end+1) = ctxsm.parcellationlabel(ctxsm.parcellation(dsearchn(ctxsm.pos,pos)));
    ctxsm.areas.parcels = [ctxsm.areas.parcels output_txt(end)];
    ctxsm.areas.coords = [ctxsm.areas.coords; pos(1:3)];
    if not(isfield(ctxsm,'nrm'))
        % compute normals
        ctxsm.nrm = normals(ctxsm.pos, ctxsm.tri);
        set(gcf,'userdata',{ctxsm vtx});
    end
    idx = dsearchn(ctxsm.pos,pos);
    dippos = ctxsm.pos(idx,:);
    adippos = ctxsm.allpos(idx,:,:);
    dipmom = ctxsm.nrm(idx,:);
    adipmom = ctxsm.allnrm(idx,:,:);
    output_txt{end+1} = num2str(idx);
    ax = {'top','left','med','back','bottom'};
    for i = 1:numel(ax)
        set(gcf,'CurrentAxes',findobj(gcf,'tag',ax{i}))
        hold on
        try
            if dodel
                delete(findobj(gca,'-regexp','tag',['dip\d+']))
            end
        end
        h = quiver3(dippos(1),dippos(2),dippos(3),dipmom(1),dipmom(2),dipmom(3),.05,'r','linewidth',2,'maxheadsize',1);
        set(h,'tag',['dip' num2str(n)])
        h = quiver3(repmat(dippos(1),size(adippos(1,1,:))),repmat(dippos(2),size(adippos(1,2,:))),repmat(dippos(3),size(adippos(1,3,:))),adipmom(1,1,:),adipmom(1,2,:),adipmom(1,3,:),.02,'k','linewidth',1,'maxheadsize',.5,'HitTest','on');
        %         h = quiver3(adippos(1,1,:),adippos(1,2,:),adippos(1,3,:),adipmom(1,1,:),adipmom(1,2,:),adipmom(1,3,:),5,'k','linewidth',1,'maxheadsize',.5);
        set(h,'tag',['dip' num2str(n)])
    end
    output_txt{end+1} = num2str(vtx(idx));
end
todisp = regexprep(output_txt,'[XYZ]: ','');
todisp = sprintf('%s    ',todisp{:});
disp(todisp)


function ctx = removeunknown(ctx)


