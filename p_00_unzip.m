% Here, if this was not done before, we list zip files of the HCP project
% and unzip some of them to a destination directory.
% Only a subset of the files are unzipped to save disk space.
% Alternatively, all downloaded zip files can be unzipped to a destination
% directory.
%
% NB: zip files can be downloaded following instructions at
% https://db.humanconnectome.org

p_setpath


% what data to unzip
restin  = 1;    % MEG pipeline, resting state MEG data
structs = 1;    % structural pipeline
anat    = 1;    % MEG pipeline, anatomy


rootdirHCP = '/network/lustre/iss01/cenir/analyse/meeg/99_OPEN_DATASETS/HCP';

rootdir_Struct  = rootdirHCP;
rootdir_Anat    = rootdirHCP;
rootdir_Restin  = rootdirHCP;

destdir = '~/analyse_max/SOLID_MEEG/replicate/HCPunzipped';
mkdir(destdir);

if restin
    zips_Restin = flister('(?<ds>\d{6})_MEG_Restin_preproc.zip$','dir',rootdir_Restin);
    goto(destdir)
    for i = 1:numel(zips_Restin)
        % note this was run on a linux machine.
        system(['unzip -n ' zips_Restin(i).name ' *MEG_?-Restin_rmegpreproc.mat']);
        system(['unzip -n ' zips_Restin(i).name ' *MEG_?-Restin_baddata_badchannels.txt']);
        system(['unzip -n ' zips_Restin(i).name ' *MEG_?-Restin_baddata_badsegments.txt']);
    end
    goback
end

if anat
    zips_Anat = flister('(?<ds>\d{6})_MEG_anatomy.zip$','dir',rootdir_Anat);
    goto(destdir)
    for i = 1:numel(zips_Anat)
        system(['unzip -n ' zips_Anat(i).name ' *MEG_anatomy_headmodel.mat']);
        system(['unzip -n ' zips_Anat(i).name ' *MEG_anatomy_transform.txt']);
        system(['unzip -n ' zips_Anat(i).name ' *.midthickness.4k_fs_LR.surf.gii']);
    end
    goback
end

if structs
    zips_Struct = flister('(?<ds>\d{6})_3T_Structural_preproc.zip$','dir',rootdir_Struct);
    goto(destdir)
    for i = 1:numel(zips_Struct)
        %% orig, superhigh def
        system(['unzip -n ' zips_Struct(i).name ' *L.aparc.a2009s.native.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *R.aparc.a2009s.native.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *T1w/Native/*L.midthickness.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *T1w/Native/*R.midthickness.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/Native/*L.midthickness.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/Native/*R.midthickness.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/Native/*L.sphere.MSMAll.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/Native/*R.sphere.MSMAll.native.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/*L.sphere.164k_fs_LR.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *MNINonLinear/*R.sphere.164k_fs_LR.surf.gii']);
        %% 32k
        system(['unzip -n ' zips_Struct(i).name ' *L.aparc.a2009s.32k_fs_LR.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *R.aparc.a2009s.32k_fs_LR.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *T1w/fsaverage_LR32k/*L.midthickness.32k_fs_LR.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *T1w/fsaverage_LR32k/*R.midthickness.32k_fs_LR.surf.gii']);
        %% 164k
        system(['unzip -n ' zips_Struct(i).name ' *L.aparc.a2009s.164k_fs_LR.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *R.aparc.a2009s.164k_fs_LR.label.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *L.midthickness.164k_fs_LR.surf.gii']);
        system(['unzip -n ' zips_Struct(i).name ' *R.midthickness.164k_fs_LR.surf.gii']);
    end
    goback
end
