#!/bin/bash


source ~/owncloud/Lab/Projects/SOLID_MEEG/code/workbench/add_wb


for suj in ??????
do
for hemi in L R
do
echo $suj
wb_command -surface-resample \
$suj/T1w/Native/$suj.$hemi.midthickness.native.surf.gii \
$suj/MNINonLinear/Native/$suj.$hemi.sphere.MSMAll.native.surf.gii \
$suj/MNINonLinear/$suj.$hemi.sphere.164k_fs_LR.surf.gii \
BARYCENTRIC \
$suj/T1w/$suj.$hemi.midthickness.164k_fs_LR.surf.gii 

wb_command -label-resample \
$suj/MNINonLinear/Native/$suj.$hemi.aparc.a2009s.native.label.gii \
$suj/MNINonLinear/Native/$suj.$hemi.sphere.MSMAll.native.surf.gii \
$suj/MNINonLinear/$suj.$hemi.sphere.164k_fs_LR.surf.gii \
BARYCENTRIC \
$suj/T1w/$suj.$hemi.aparc.a2009s.164k_fs_LR.label.gii


done
done
