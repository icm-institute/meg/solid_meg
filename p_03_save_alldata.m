%% here we save usefuldata from the 89 subjects as one file for easy access

p_setpath

fs = p_dbload('restin',{'name','label','ds'});

ntrimax = 200;
ntimes = 50;
nchan = 248;

textprogressbar('loading and preparing data')
alldatasize = nchan*ntimes*ntrimax*numel(fs)*4;
if alldatasize > 10e9
    error('too much data in memory?')
end
alldata = NaN(nchan,ntimes,ntrimax,size(fs,1),'single');
tmp = [];
for isuj = 1:size(fs,1)
    textprogressbar(isuj/size(fs,1)*100);
    tmp = load(fs(isuj,1).name,'trial0*','trial1*','trial200');
    trials = fieldnames(tmp);
    for itri = 1:numel(trials)
        alldata(:,:,itri,isuj) = single(tmp.(trials{itri})(:,1:ntimes));
    end
end
clear tmp
textprogressbar('done')

save(fullfile(datadir,sprintf('alldata_%dtimepoints.mat',ntimes)),'alldata')

