function last_cmd = p_send2cluster(cfg)

% this function will prepare jobs to be run on the cluster.
% it creates output directories, saves variables (shared and for individual
% jobs), creates scripts, and a batch command for the SLURM cluster engine.
%
% cfg.jobfilename   = string: file name of job files
% cfg.jobnickname   = string: nickname for job (optional, defaults to jobfilename)
% cfg.copyscripts   = boolean: whether to copy scripts in scriptsdir to the
%                   server before start (default is true)
% cfg.scriptsdir    = string: path to scripts
% cfg.scriptsdir2exclude = cellstr of directories to exclude from copy
%                          to server (i.e. already accessible there)
% cfg.jobsdir       = string: path to job dir (accessible on cluster)
% cfg.vars2save     = structure array with constants to save to disk
% cfg.var2slice     = structure array containing a variable to slice (i.e.
%                       to run separate jobs on each slice of that variable
%                       along slicedim)
% cfg.slicedim      = numeric: dimension of var2slice to run separate jobs on
% cfg.njobs         = numeric: number of jobs (defaults to size(var2slice,slicedim));
% cfg.toeval        = cellstr: lines to eval before running each job
%
% cfg.onlycommand   = don't do anything but creating the command line
% cfg.doithere      = run locally now
% cfg.debug         = when doithere is true, stop in the first line of jobfilename
%                       default = true
% cfg.createjobscripts = boolean: whether or not to write job scripts
% cfg.cleardirs     = boolean: whether or not output directories should be
%                       cleared before running
%
% cfg.sbatchcfg = configuration structure for sbatch command with fields
%                   shown below with default values (see man sbatch on cluster for more
%                   information):
%         partition = 'normal';
%         timelimit = '01:00:00';
%         Nodes = '1';
%         cpus_per_task = '1';
%         o = fullfile(cfg.jobsdir, 'logs/log-%a');
%         e = fullfile(cfg.jobsdir,'/err/err-%a');
%         array = ['1-' num2str(cfg.njobs)];
%         cmd = ['"' fullfile(cfg.jobsdir,'/0runjobs.bash'),'"'];


name2slice = fieldnames(cfg.var2slice);
if not(isfield(cfg,'jobnickname'))
    cfg.jobnickname = cfg.jobfilename;
end
if not(isfield(cfg,'njobs'))
    cfg.njobs = size(cfg.var2slice.(name2slice{1}),cfg.slicedim);
end
if not(isfield(cfg,'scriptsdir2exclude'))
    cfg.scriptsdir2exclude = {'fieldtrip' 'miscMatlab' 'megconnectome' 'db'  '*.err' '*.out'};
end
if not(isfield(cfg,'scriptsdir2link'))
    cfg.scriptsdir2link = {'fieldtrip' 'miscMatlab'};
end
if not(isfield(cfg,'copyscripts'))
    cfg.copyscripts = 1;
end
if not(isfield(cfg,'createjobscripts'))
    cfg.createjobscripts = 1;
end
if not(isfield(cfg,'createbatchscript'))
    cfg.createbatchscript = 1;
end
if not(isfield(cfg,'cleardirs'))
    cfg.cleardirs = 1;
end
if not(isfield(cfg,'sbatchcfg'))
    cfg.sbatchcfg = [];
end
if not(isfield(cfg,'onlycommand'))
    cfg.onlycommand = 0;
end
if not(isfield(cfg,'doithere'))
    cfg.doithere = 0;
end
if not(isfield(cfg,'debug'))
    cfg.debug = 1;
end
scriptsdir = fullfile(cfg.jobsdir,'scripts/');

if ~cfg.onlycommand
    %% preparing results (output, logs, errors) directories
    if any([cfg.cleardirs,cfg.copyscripts])
        fid = fopen('tmp.sh','wt');
        fprintf(fid,['#!/bin/bash\n']);
    end
    if cfg.cleardirs
        fprintf(fid,['mkdir -p ' cfg.jobsdir '\n']);
        fprintf(fid,['mkdir ' fullfile(cfg.jobsdir,'logs')  '\n']);
        fprintf(fid,['mkdir ' fullfile(cfg.jobsdir,'err')  '\n']);
        fprintf(fid,['rm -rf ' fullfile(cfg.jobsdir,'logs/*')  '\n']);
        fprintf(fid,['rm -rf ' fullfile(cfg.jobsdir,'err/*')  '\n']);
        fprintf(fid,['mkdir ' fullfile(cfg.jobsdir,'scripts')  '\n']);
    end
    if cfg.copyscripts
        %% rsync all scripts to job dir
        fprintf('rsyncing scripts...')
        toexclude = ['--exclude ".git" '];
        for i = 1:numel(cfg.scriptsdir2exclude)
            toexclude = [toexclude '--exclude ''' cfg.scriptsdir2exclude{i} ''' '];
        end
        fprintf(fid,['rsync -rtpg --delete --progress ' toexclude cfg.scriptsdir '/* ' fullfile(cfg.jobsdir,'scripts/')]);
        fprintf(fid,'\n');
        for i = 1:numel(cfg.scriptsdir2link)
            fprintf(fid,['ln -s ' fullfile(cfg.scriptsdir,cfg.scriptsdir2link{i}) ' -T ' fullfile(cfg.jobsdir,'scripts',cfg.scriptsdir2link{i}) '\n']);
        end
        fclose(fid);
    end
    if any([cfg.cleardirs,cfg.copyscripts])
        !chmod +x tmp.sh
        [s,co] = system('./tmp.sh');
        fprintf('Done\n')
    end
    %% create all scripts
    if cfg.createjobscripts
        fprintf('Creating all job scripts...')
        
        vars2save = cfg.vars2save;
        save(fullfile(cfg.jobsdir,'clusterjob_common.mat'),'-struct','vars2save','-v7.3','-nocompression')
        
        for ij = 1:cfg.njobs
            str = [name2slice{1} '= cfg.var2slice.(name2slice{1})('];
            for i = 1:ndims(cfg.var2slice.(name2slice{1}))
                if i == cfg.slicedim
                    str = [str, 'ij,'];
                else
                    str = [str, ':,'];
                end
                if i == ndims(cfg.var2slice.(name2slice{1}))
                    str(end:end+1) = ');';
                end
            end
            eval(str);
            
            save(fullfile(cfg.jobsdir,sprintf('clusterjob%03d.mat',ij)),name2slice{1})
            
            script2job = {['cd ' scriptsdir]
                [cfg.jobfilename '(' num2str(ij) ');']};
            
            fid = fopen(fullfile(cfg.jobsdir,sprintf([cfg.jobfilename '_%03d.m'],ij)),'wt');
            fprintf(fid,'%s\n',script2job{:});
            fclose(fid);
        end
        fprintf('Done\n')
    end
    if cfg.createbatchscript
        tmp = [];
        tmp.mem = '1G';
        tmp.partition = 'normal,bigmem';
        tmp.timelimit = '01:00:00';
        tmp.Nodes = '1';
        tmp.cpus_per_task = '1';
        tmp.job_name = cfg.jobnickname;
        tmp.o = fullfile(cfg.jobsdir, 'logs/log-%a');
        tmp.e = fullfile(cfg.jobsdir,'/err/err-%a');
        tmp.array = ['1-' num2str(cfg.njobs)];
        tmp.cmd = [fullfile(cfg.jobsdir,'/0runjobs.bash')];
        
        tmp = setdef(cfg.sbatchcfg,tmp);
        
        str = {'#!/bin/bash'
            ['#SBATCH --mem=' tmp.mem]
            ['#SBATCH --partition=' tmp.partition ]
            ['#SBATCH --time=' tmp.timelimit ]
            ['#SBATCH --nodes=' tmp.Nodes ]
            ['#SBATCH --cpus-per-task=' tmp.cpus_per_task ]
            ['#SBATCH --job-name=' tmp.job_name ]
            ['#SBATCH --output=' tmp.o ]
            ['#SBATCH --error=' tmp.e ]
            ['#SBATCH --array=' tmp.array ]
            ''
            'echo started on $HOSTNAME'
            'date'
            'module load MATLAB'
            'tic="`date +%s`"'
            ['cmd=$( printf "' cfg.jobfilename '_%03d" ${SLURM_ARRAY_TASK_ID})']
            ['/network/lustre/iss01/apps/lang/matlab/R2017b/bin/matlab -nodesktop -nodisplay -nosoftwareopengl -r "cd ' cfg.jobsdir '; $cmd"']
            'let toc="`date +%s`"'
            'let sec="$toc - $tic"'
            'let heu="$sec / 3600"'
            'let min="($sec - $heu * 3600) / 60"'
            'echo Elapsed time: $heu H : $min min'};
        
        fid = fopen(fullfile(cfg.jobsdir,'0runjobs.bash'),'wt');
        
        fprintf(fid,'%s\n',str{:});
        fclose(fid);
    end
end

if cfg.doithere
    cd(scriptsdir)
    if cfg.debug
        dbstop('in',cfg.jobfilename)
    end
    for i = 1:cfg.njobs
        eval([cfg.jobfilename '(' num2str(i) ');']);
    end
else
    str = ['sbatch  ' tmp.cmd ];
end
if nargout == 0
    disp('now ssh to login1:')
    disp('ssh login01')% 10.5.90.1
    disp('and run:')
    disp(str)
else
    last_cmd = str;
end
