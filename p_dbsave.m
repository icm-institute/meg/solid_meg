function p_dbsave(fs,db,f)

% save fields to database 
% dbsave(fs,dbname,f)
% fs = database data to save
% dbname = database name to save to (defaults to 'restin')
% f = field names to save (default = fieldnames(fs))

try
    dbdir = getpref('solid','dbdir');
catch
    dbdir = fullfile(cd,'db');
end
if not(exist('db','var'))
    db = 'restin';
end
if not(exist('f','var'))
    f = fieldnames(fs)';
end
if ischar(f)
    f = {f};
end
if not(exist(dbdir,'dir'))
    mkdir(dbdir)
end

for f = f
    eval([f{1} '= reshape({fs.(''' f{1} ''')},size(fs));']);
    s = whos(f{1});
    if s.bytes > 2e9
        save(fullfile(dbdir,[db '_' f{1},'.mat']),f{1},'-v7.3');
    else
        save(fullfile(dbdir,[db '_' f{1},'.mat']),f{1});
    end
end
