function p_figure03_jobs(ijob)

addpath(genpath(fullfile('/network/lustre/iss01/home/maximilien.chaumon/','cluster_MATLAB','general')));
addpath(fullfile(resolvePath('~'),'cluster_MATLAB','fieldtrip'));
addpath(fullfile(resolvePath('~'),'cluster_MATLAB','megconnectome-3.0'));

load('../../common.mat','allctxsm','allgrad','channeighbstructmat','alldata');
load('../clusterjob_common.mat','fs','specif');
[fs.ctxsm] = rep2struct(allctxsm);
[fs.grad] = rep2struct(allgrad);
struct2ws(specif)

load(sprintf('../clusterjob%03d.mat',ijob),'MCs')
if not(isfield(MCs,'times'))
    error('Need times specification for this job')
end

nchan = size(alldata,1);
ntimes = numel(MCs(1).times);
fprintf('job%03d',ijob)
rng('shuffle')

for iMC = 1:numel(MCs)
    % find maxsensor across subjects of this MC
    clear alldipdat
    for i = 1:numel(MCs(iMC).sujs)
        alldipdat(:,:,i) = fs(MCs(iMC).sujs(i)).lf * fs(MCs(iMC).sujs(i)).dipmom * amp;
    end
    [~,maxsensor] = maxabs(mean(alldipdat,3));
    % dimensions:
    % sensors, time, subjects;
    tic
    d1 = NaN([nchan,ntimes,numel(MCs(iMC).sujs)]);
    d2 = NaN(size(d1));
    for isuj = 1:numel(MCs(iMC).sujs)

        itri = 1:size(MCs(iMC).tris,2)/2;% first half of trials
        tmp = alldata(:,MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
        tmp = nanmean(tmp,3); % avg across trials
        d1(:,:,isuj) = tmp;

        itri = (size(MCs(iMC).tris,2)/2+1):size(MCs(iMC).tris,2);% second half of trials
        tmp = alldata(:,MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
        tmp = nanmean(tmp,3); % avg across trials
        d2(:,:,isuj) = tmp;

        d2(:,:,isuj) = d2(:,:,isuj) + alldipdat(:,:,isuj);
        if 0
            %%
            tmp = [];
            tmp.avg = alldipdat(:,:,isuj);
            tmp.time = 1;
            tmp.label = fs(isuj).grad.label(chnb('meg',fs(isuj).grad.label));
            tmp.grad = fs(isuj).grad;
            tmp.dimord = 'chan_time';

            figure(6868);clf;
            cfg = [];
            cfg.layout = '4D248.lay';
            cfg.zlim = 'maxabs';
            ft_topoplotER(cfg,tmp);
            drawnow
        end
    end
    dd1 = d1(:,1,:);% take just one time point (like originally)
    dd2 = d2(:,1,:);
    d1 = mean(d1,2);% mean across ntimes samples
    d2 = mean(d2,2);
    totest = d1-d2;
    ttotest = dd1 - dd2;
    [h,p,~,stat] = ttest(totest,0,'Dim',3);
    [hh,pp,~,sstat] = ttest(ttotest,0,'Dim',3);
    MCs(iMC).m = [];%mean(totest,3);
    MCs(iMC).h = logical(h);
    MCs(iMC).p = single(p);
    MCs(iMC).tstat = single(stat.tstat);
    MCs(iMC).df = stat.df(1);
    MCs(iMC).sd = [];%single(stat.sd);
    MCs(iMC).hany = any(h);
    MCs(iMC).h = h(maxsensor);
    MCs(iMC).hh = logical(hh(maxsensor));
    MCs(iMC).maxsensor = maxsensor;

    if doperm
        % permute tests
        s = size(totest); ss = s(3); s(3) = 1;
        pperm = NaN([s(1:2) nperm]); tperm = NaN(size(pperm));
        for iperm = 1:nperm
            thisperm = reshape([rem(randperm(ss),2) * 2-1],[1 1 ss]);
            thispermtotest = (d1-d2) .* repmat(thisperm,s);

            [h,p,~,stat] = ttest(thispermtotest,0,'Dim',3);
            pperm(:,:,iperm) = p;
            tperm(:,:,iperm) = single(stat.tstat);
        end
        % cluster
        cfg = [];
        cfg.StatPerm = tperm.^2;
        cfg.pperm = pperm;
        cfg.Stat = MCs(iMC).tstat.^2;
        cfg.p = MCs(iMC).p;
        cfg.clustpthresh = clustthresh;
        cfg.finalpthresh = finalpthresh;
        cfg.channeighbstructmat = channeighbstructmat;
        [stats] = clusterstats(cfg);
        % we will consider significant only clusters that include at
        % the peak sensor of the dipole projection
        MCs(iMC).hperm = stats.mask(maxsensor);
        MCs(iMC).hperm2 = any(stats.mask);
    end
    toc
    if 0
        %%
        tmp = [];
        tmp.avg = mean(totest,3);%mean(d2,3);%-log10(MCs(iMC).p);%mean(toto,2);%dipdat;%%mean(totest,3);
        tmp.time = 1;
        tmp.label = fs(isuj).grad.label(chnb('meg',fs(isuj).grad.label));
        tmp.grad = fs(isuj).grad;
        tmp.dimord = 'chan_time';

        figure(6868);clf;
        cfg = [];
        cfg.layout = '4D248.lay';
        cfg.zlim = 'maxabs';
        ft_topoplotER(cfg,tmp);
        drawnow
    end
end
save(sprintf('../clusterjob%03d.mat',ijob),'MCs')
