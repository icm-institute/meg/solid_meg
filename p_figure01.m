
% this script plots all panels of Figure 1

p_setpath

fs = p_dbload('restin',{'name','ds','atlas32','grad','hm','label'});

% use atlas32 as cortex source model
fs = renamefields(fs,'atlas32','ctxsm');
% compute normal orientations
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end
% load all trials
load(fullfile(datadir,'alldata_50timepoints.mat'))

figdir = fullfile(figsdir,'Figure1');mymkdir(figdir)

load layout

%% Panel a: Head model, dipole and topography
isuj = 1;
dipamp = 10;
seed = 5276;
fs(isuj).dippos =    fs(isuj).ctxsm.pos(seed,:);
fs(isuj).dipmom = fs(isuj).ctxsm.nrm(seed,:)';

fig(12,3390);clf
subplot(121)
ft_plot_headmodel(fs(isuj).hm,'vertexcolor','none','facecolor','skin','edgecolor','none','facealpha',.5);
hold on
% source model (cortex)
ft_plot_mesh(fs(isuj).ctxsm,'facealpha',.5)
tmp = fs(isuj).ctxsm;
tmpgrad = fs(1).grad;
[sel,meglabels] = chnb(fs(1).label,tmpgrad.label);
tmpgrad.label = tmpgrad.label(sel);
tmpgrad.chanori = tmpgrad.chanori(sel,:);
tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
tmpgrad.chantype = tmpgrad.chantype(sel);
tmpgrad.chanunit = tmpgrad.chanunit(sel);
tmpgrad.tra = tmpgrad.tra(sel,:);
ft_plot_sens(tmpgrad)

quiver3(fs(isuj).dippos(1),fs(isuj).dippos(2),fs(isuj).dippos(3),fs(isuj).dipmom(1),fs(isuj).dipmom(2),fs(isuj).dipmom(3),.05,'r','linewidth',2,'maxheadsize',2)
view(-90,90)
lighting gouraud
material dull
camlight

subplot(122)
fs(isuj).lf = ft_compute_leadfield(fs(isuj).dippos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
topo = fs(isuj).lf * fs(isuj).dipmom * dipamp;
tmp = [];
tmp.avg = topo;
tmp.time = 1;
tmp.label = fs(isuj).label;
tmp.grad = fs(isuj).grad;
tmp.dimord = 'chan_time';

cfg = [];
cfg.layout = layout;
cfg.zlim = maxabs(topo,'-+');
cfg.comment = 'no';
cfg.gridscale = 200;
cfg.interactive = 'no';
ft_topoplotER(cfg,tmp);
colormap(gca,varycolor(256,{'blue','white','red'}));
drawnow

export_fig(fullfile(figdir,'HeadModel_and_Topo.png'),'-r300','-nocrop','-transparent');


%% panel b: noise, signal, topo, average
% topo one more time
figure(38738);clf
colormap(gca,varycolor(256,{'blue','white','red'}));
ft_topoplotER(cfg,tmp);
drawnow

% noise
tmp = [];
tmp.time = 1;
tmp.label = fs(isuj).label;
tmp.grad = fs(isuj).grad;
tmp.dimord = 'chan_time';
cfg = [];
cfg.layout = layout;
cfg.zlim = maxabs(alldata(:,:,1,isuj),'-+');
cfg.comment = 'no';

for ipoint = 1:10
    tmp.avg = alldata(:,ipoint,1,isuj);
    
    figure(6868);clf;
    ft_topoplotER(cfg,tmp);
    colormap(gca,varycolor(256,{'blue','white','red'}));
    drawnow

    export_fig(fullfile(figdir,sprintf('RestingNoise_%03d.png',ipoint)),'-r300','-nocrop','-transparent');
%     set(gcf,'paperpositionmode','auto')
%     print('-dpng','-r300',fullfile(fig1dir,sprintf('RestingNoise_%03d.png',ipoint)))

end


%% topos average
% noise
tmp = [];
tmp.avg = mean(alldata(:,1:5,1,isuj),2);
tmp.time = 1;
tmp.label = fs(isuj).label;
tmp.grad = fs(isuj).grad;
tmp.dimord = 'chan_time';

figure(6868);clf;
cfg = [];
cfg.layout = layout;
cfg.zlim = maxabs(alldata(:,:,1,isuj),'-+');
cfg.comment = 'no';
%     cfg.gridscale = 200;
ft_topoplotER(cfg,tmp);
    colormap(gca,varycolor(256,{'blue','white','red'}));
drawnow
export_fig(fullfile(figdir,'RestingNoise_avg.png'),'-r300','-nocrop','-transparent');

% signal
illustration = 7; % larger amplitude for illustration
tmp.avg = mean(alldata(:,6:10,1,isuj)+topo * illustration,2); % 

figure(6868);clf;
ft_topoplotER(cfg,tmp);
    colormap(gca,varycolor(256,{'blue','white','red'}));
drawnow
export_fig(fullfile(figdir,'RestingNoiseSignal_avg.png'),'-r300','-nocrop','-transparent');


%% Panel c is a copy of Figure 16e (insula)

