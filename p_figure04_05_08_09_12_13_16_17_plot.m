% figures 3 7 11 15 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','atlas32','grad','hm','transform'});

fs = renamefields(fs,'atlas32','ctxsm');
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end

p_neighb

root_jobsdir = fullfile(jobsdir,'figure4');
figdir = fullfile(figsdir,'Figure4');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

mctxsm = p_create_meanctx('32',fs);

load layout


%% simulation parameters
% number of resamples
nMC = 500;

% benchmark of subjects and trials
nsujs = 5:5:50;
nnsujs = numel(nsujs);
ntris = [2 20:20:200];    % keep rem(ntris,2) == 0
nntris = numel(ntris);
ntimepoints = 50;

dipamp = 10;

p_all_areas

%% collect responses
% computations have been done. Now need to gather results

perf = cell(1,numel(all_areas.parcels));
disp(root_jobsdir)
for i_ar = 1:2%:numel(all_areas.parcels)
    disp(all_areas.parcels{i_ar})
    % gather results
    [MCs] = p_gather_clusterjobs(fullfile(root_jobsdir, [num2str(i_ar,'%02d') '_' all_areas.parcels{i_ar}]));
    
    booboots = struct2table(MCs(:));
    booboots.sujs = cellfun(@numel,booboots.sujs);
    booboots.tris = cellfun(@(x) size(x,2),booboots.tris);
    booboots.area = repmat(all_areas.parcels(i_ar),size(booboots,1),1);
    booboots.seed = repmat(all_areas.seeds(i_ar),size(booboots,1),1);
    booboots.amp = repmat(dipamp,size(booboots,1),1);
    booboots.i_ar = repmat(i_ar,size(booboots,1),1);

    % remove useless variables
    vs = booboots.Properties.VariableNames;
    v = vs(regexpcell(vs,{'tstat','df','sd','m','p','m','times'},'exact'));
    booboots(:,v) = [];
    
    perf{i_ar} = grpstats(booboots,{'sujs','tris','area'},'sum','DataVars','hperm');
    perf{i_ar}.pperm = perf{i_ar}.sum_hperm ./ perf{i_ar}.GroupCount;
end


%% figures showing:
% head model in 3 orientations
% histogram of spatial and momentum distributions
% topography
% power plot
fignums = [4 5 8 9 12 13 16 17];ifig = 0;
% all_areas in figure order:
figureorder = [12 10 9 2 7 4 6 3];

for i_ar = figureorder
    %%
    for isuj = 1:size(fs,1)
        %
        
        list = all_areas.seeds(i_ar);
        
        fs(isuj).dippos = mean(fs(isuj).ctxsm.pos(list,:),1);
        fs(isuj).dipmom = mean(fs(isuj).ctxsm.nrm(list,:),1)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
        
    end
    
    fig(43,4777+i_ar);clf
    % whole brain view
    annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.97 0.0249 0.0271],'String','a');
    axes('position',[0.01    0.73    0.1958    0.2953])
    ft_plot_mesh(mctxsm,'facecolor',colors('grey50'))
    hold on
    spacelim = [-.04 .04];
    allpos = vertcat(fs.dippos);
    allmom = horzcat(fs.dipmom)';
    quiver3(allpos(:,1),allpos(:,2),allpos(:,3),allmom(:,1),allmom(:,2),allmom(:,3),2,'color',colors('red'),'linewidth',.1,'maxheadsize',.1,'autoscale','off')
    mmom = mean(allmom,1) .* spacelim(2);% normalize momentum of the mean by plot limits
    quiver3(mean(allpos(:,1)),mean(allpos(:,2)),mean(allpos(:,3)),mmom(1),mmom(2),mmom(3),1,'color',colors('black'),'linewidth',2,'maxheadsize',.5,'autoscale','off')
    set(gca,'tag','brain')
    lighting gouraud
    material dull
    view(all_areas.view(i_ar,1),all_areas.view(i_ar,2))
    camlight('headlight')
    rotate3d on
    
    % detail views
    axes('position',[0.2692    0.73    0.1958    0.2953],'fontsize',14)
    ft_plot_mesh(mctxsm,'facecolor',colors('grey50'),'facealpha',.2)
    hold on
    spacelim = [-.04 .04];
    allpos = vertcat(fs.dippos);
    allmom = horzcat(fs.dipmom)';
    quiver3(allpos(:,1),allpos(:,2),allpos(:,3),allmom(:,1),allmom(:,2),allmom(:,3),2,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','off')
    mmom = mean(allmom,1) .* spacelim(2);% normalize momentum of the mean by plot limits
    quiver3(mean(allpos(:,1)),mean(allpos(:,2)),mean(allpos(:,3)),mmom(1),mmom(2),mmom(3),1,'color',colors('black'),'linewidth',2,'maxheadsize',.5,'autoscale','off')
    axis([ -0.1115    0.0835   -0.0993    0.0997   -0.0687    0.1263])
    lims = spacelim' + mean(allpos);
    xlim(lims(:,1))
    ylim(lims(:,2))
    zlim(lims(:,3))
    xlabel x;ylabel y;zlabel z
    axis on
    set(gca,'tag','brain_detail_top')
    lighting gouraud
    material dull
    view(-90,90)
    camlight
    rotate3d on
    
    axes('Position',[ 0.5350    0.73    0.1958    0.2953],'fontsize',14);
    ft_plot_mesh(mctxsm,'facecolor',colors('grey50'),'facealpha',.2)
    hold on
    spacelim = [-.04 .04];
    allpos = vertcat(fs.dippos);
    allmom = horzcat(fs.dipmom)';
    quiver3(allpos(:,1),allpos(:,2),allpos(:,3),allmom(:,1),allmom(:,2),allmom(:,3),2,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','off')
    mmom = mean(allmom,1) .* spacelim(2);% normalize momentum of the mean by plot limits
    quiver3(mean(allpos(:,1)),mean(allpos(:,2)),mean(allpos(:,3)),mmom(1),mmom(2),mmom(3),1,'color',colors('black'),'linewidth',2,'maxheadsize',.5,'autoscale','off')
    axis([ -0.1115    0.0835   -0.0993    0.0997   -0.0687    0.1263])
    lims = spacelim' + mean(allpos);
    xlim(lims(:,1))
    ylim(lims(:,2))
    zlim(lims(:,3))
    xlabel x;ylabel y;zlabel z
    axis on
    set(gca,'tag','brain_detail_left')
    lighting gouraud
    material dull
    view(180,0)
    camlight
    rotate3d on
    %
    axes('Position',[ 0.8007    0.73    0.1958    0.2953],'fontsize',14)
    ft_plot_mesh(mctxsm,'facecolor',colors('grey50'),'facealpha',.2)
    hold on
    spacelim = [-.04 .04];
    allpos = vertcat(fs.dippos);
    allmom = horzcat(fs.dipmom)';
    quiver3(allpos(:,1),allpos(:,2),allpos(:,3),allmom(:,1),allmom(:,2),allmom(:,3),2,'color',colors('red'),'linewidth',1,'maxheadsize',.5,'autoscale','off')
    mmom = mean(allmom,1) .* spacelim(2);% normalize momentum of the mean by plot limits
    quiver3(mean(allpos(:,1)),mean(allpos(:,2)),mean(allpos(:,3)),mmom(1),mmom(2),mmom(3),1,'color',colors('black'),'linewidth',2,'maxheadsize',.5,'autoscale','off')
    axis([ -0.1115    0.0835   -0.0993    0.0997   -0.0687    0.1263])
    lims = spacelim' + mean(allpos);
    xlim(lims(:,1))
    ylim(lims(:,2))
    zlim(lims(:,3))
    xlabel x;ylabel y;zlabel z
    axis on
    set(gca,'tag','brain_detail_back')
    lighting gouraud
    material dull
    view(-90,0)
    camlight
    rotate3d on
    
    % histogram of position variability
    annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.68 0.0249 0.0271],'String','b');
    axes('Color','none', 'ColorOrder',varycolor(size(allpos,2)),'position',[0.0685    0.4957    0.5521    0.1584]);%,[0.0685    0.4263    0.6    0.2578]);
    set(gca,'fontsize',14)
    hold on
    allpos = vertcat(fs.dippos);
    mpos = mean(allpos,1);
    allpos = allpos - mpos;
    [c,ce] = hist(allpos*1000,linspace(-30, 30,20));
    bar(ce,c);
    xlabel('distance (mm)')
    ylabel('count')
    set(gca,'tag','histo_pos')
    legend({'x','y','z'},'box','off','position',[0.1033    0.5552    0.0603    0.1020]);%,[0.1062    0.5644    0.0565    0.1020])
    xlim(maxabs(ce,'+-'))
    box off
    
    % histograms of orientation variability
    annotation('textbox','fontsize',20,'linestyle','none','position',[0.64 0.68 0.0249 0.0271],'String','c');
    set(axes,'Position',[0.7426    0.4623    0.2055    0.2055]);%,[0.6736    0.4010    0.2500    0.2500])
    allmom = horzcat(fs.dipmom)';
    norm(mean(allmom,1))
    [allmomsph(:,1),allmomsph(:,2)] = cart2sph(allmom(:,1),allmom(:,2),allmom(:,3));
    mmom = circ_mean(allmomsph,[],1);
    allmomsph = allmomsph- mmom;
    rd = acos(abs(mctxsm.radiality(all_areas.seeds(i_ar))));
    allmomsph = allmomsph + rd;
    for i = 1:size(allmomsph,2)
        polarhistogram(allmomsph(:,i),linspace(-pi,pi,40));
        hold on
    end
    tmp = get(gca,'RLim');
    h = polarplot(gca,[rd rd],[0 norm(mean(allmom,1)) * tmp(2)],'k','linestyle','-','linewidth',2);
    set(gca,'ThetaDir','clockwise','ThetaZeroLocation','top','RAxisLocation',330,'FontSize',16)
    Tt = [0:pi/6:2*pi rd] / pi * 180;
    [Tt,idx] = sort(Tt);
    Ttl = {'' '' '' '' '' '' '' '' '' '' '' '' '' [num2str(round((rd)/pi*180)) '°']};Ttl = Ttl(idx);
    set(gca,'ThetaTick',Tt,'ThetaTickLabel',Ttl)
    set(gca,'Rtick',linspace(tmp(1),tmp(2),5),'Rticklabel','')
    text(gca,3*pi/2,diff(tmp)/5,'Tangential','FontSize',get(gca,'FontSize'),'HorizontalAlignment','right','VerticalAlignment','middle')
    text(gca,2*pi/2,2*diff(tmp)/5,'Radial','FontSize',get(gca,'FontSize'),'HorizontalAlignment','right','VerticalAlignment','middle','rotation',90)
    Rl = get(gca,'RLim');
    h = polarplot([pi/2 pi/2],[Rl],'k:');
    h(2) = polarplot([3*pi/2 3*pi/2],[Rl(1) diff(tmp)/5],'k:');
    h(3) = polarplot([pi pi],[Rl(1) 2 * diff(tmp)/5 ],'k:');
    h(4) = polarplot([0 0],[Rl],'k:');
    
    set(gca,'tag','histo_ori')
    legend({'azimuth','elevation'},'box','off','position',[ 0.66    0.58    0.1383    0.0852]);%,[ 0.58    0.5888    0.1296    0.0793])
    box off
    
    % topo
    annotation('textbox','fontsize',20,'linestyle','none','position',[0.006 0.41 0.0249 0.0271],'String','d');
    axes;
    clear lftopo
    for isuj = 1:size(fs,1)
        fs(isuj).lf = ft_compute_leadfield(fs(isuj).dippos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
        fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
        fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
        lftopo(:,:,isuj) = fs(isuj).lf * fs(isuj).dipmom * dipamp;
    end
    lftopo = mean(lftopo,3)*1e15;
    [m,maxsensor] = maxabs(lftopo);
    tmp = [];
    tmp.avg = lftopo;
    tmp.time = 1;
    tmp.label = fs(isuj).label;
    % plot grad of 1st subject
    tmpgrad = fs(1).grad;
    [sel,meglabels] = chnb('meg',tmpgrad.label);
    tmpgrad.label = tmpgrad.label(sel);
    tmpgrad.chanori = tmpgrad.chanori(sel,:);
    tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
    tmpgrad.chantype = tmpgrad.chantype(sel);
    tmpgrad.chanunit = tmpgrad.chanunit(sel);
    tmpgrad.tra = tmpgrad.tra(sel,:);
    tmp.grad = tmpgrad;%fs(isuj).grad;
    tmp.dimord = 'chan_time';
    % projection to sensors
    cfg = [];
    cfg.layout = layout;
    if isempty(all_areas.topoclim{i_ar})
        cfg.zlim = [min(-m,-30) max(m,30)];%'maxabs'; %[-3.76 3.76]*1e-13;
    else
        cfg.zlim = all_areas.topoclim{i_ar};
    end
    cfg.interactive = 'no';
    cfg.comment = 'no';
    cfg.gridscale = 200;
    ft_topoplotER(cfg,tmp);
    set(gca,'position',[0.1345    0.1318    0.2429    0.2555]);%,[0.12   0.03    0.33 .33]);
    set(gca,'tag','topo')
    colormap(gca,varycolor(256,{'blue','white','red'}));
    h = colorbar('fontsize',16,'position',[0.0906    0.1318    0.0201    0.2555]);%,[0.0906    0.0427    0.0200    0.3000]);
    ylabel(h,'Amplitude (fT)');
    
    % power plot
    annotation('textbox','fontsize',20,'linestyle','none','position',[0.42 0.41 0.0249 0.0271],'String','e');
    set(axes,'FontSize',14,'position',[0.4686    0.10137    0.4059    0.2958]);%,[0.5200    0.0619    0.3700    0.2700])
    colormap(gca,varycolor(256,'Moreland_Black_Body'))
    hold on

    toplot = reshape(perf{i_ar}.pperm,nntris,nnsujs);
    imagesc(nsujs,ntris,toplot,[0 1])
    levs = [.1 .5 .8];
    [xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));

    nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
    contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
    set(gca,'layer','top')
    box on
    axis xy
    xlabel('Number of subjects')
    xticks(nsujs)
    xticklabels(nsujs);
    ylabel('Number of trials')
    yticks(ntris)
    yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
    set(gca,'tag','power_contour')
    h = colorbar('position',[0.9076    0.1037    0.0196    0.2958]);%,[0.9116    0.0619    0.0196    0.27]);
    h.Label.String = 'Power';
    h.Label.FontSize =16;

    set(gcf,'name',[num2str(i_ar,'%02d') ' ' strrep(all_areas.parcels{i_ar},'L_L_','')]);
    
% end
% %% 
% for i_ar = 1:numel(all_areas.parcels)%[10 12]%
%     figure(4777+i_ar);
    %%
    set(gcf,'paperpositionmode','auto')
    drawnow
    
    ifig = ifig + 1;
    print('-dpng','-r300',fullfile(figdir,['Figure' num2str(fignums(ifig),'%02d') '_' strrep(all_areas.parcels{i_ar},'L_L_','') '.png']))
    
end
