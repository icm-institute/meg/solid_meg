% figures 3 7 11 15 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','atlas32','grad','hm','transform'});

fs = renamefields(fs,'atlas32','ctxsm');
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end

p_neighb

figdir = fullfile(figsdir,'Figure3'); mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

mctxsm = p_create_meanctx('32',fs);

% target areas for figures 4 etc.
p_all_areas
%% display table with measured properties for all locations
spacetable = table;
for i_ar = 1:numel(all_areas.parcels)
    for isuj = 1:size(fs,1)
        %
        
        list = all_areas.seeds(i_ar);
        
        fs(isuj).dippos = mean(fs(isuj).ctxsm.pos(list,:),1);
        fs(isuj).dipmom = mean(fs(isuj).ctxsm.nrm(list,:),1)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
        
    end
    % spatial variability
    allpos = vertcat(fs.dippos);
    mpos = mean(allpos,1);

    allpos = allpos - mpos;
    sdallpos = std(allpos,1);
    allmom = horzcat(fs.dipmom)';
    mmom = norm(mean(allmom,1));
    [allmomsph(:,1),allmomsph(:,2)] = cart2sph(allmom(:,1),allmom(:,2),allmom(:,3));
    allmomsph = allmomsph - circ_mean(allmomsph,[],1);
    sdallmom = circ_rad2ang(circ_std(allmomsph,[],[],1));
    clear d
    for isuj = 1:size(fs,1)
        [~,d(isuj)] = dsearchn(fs(isuj).grad.chanpos,fs(isuj).dippos);
    end
    d = mean(d)*1000;
    spacetable(i_ar,:) = {all_areas.parcels{i_ar} mctxsm.posvar(all_areas.seeds(i_ar)) mmom d rad2deg(acos(mctxsm.radiality(all_areas.seeds(i_ar))))};%,toplot(all_areas.seeds(i_ar))};
    spacetable.Properties.VariableNames = {'Region','Location_variability_mm','Resultant_vector_length','mean_Minimum_distance_mm' 'Radiality'};%,'Avg_Stat_Power'};
end
% add xyz coordinates in mm
spacetable(:,{'x' 'y' 'z'}) = mat2cells(round(all_areas.coords(:,:)*1000));

% in figure order:
figureorder = [12 10 9 2 7 4 6 3];

spacetable(figureorder,:)

%% plot ctx properties in space

cols = colors({'#d55f00' '#189a77'});
nbins = 200;
fontsize = 18;
histopts = {'EdgeColor','none'};

% distance to sensors
figure(33);clf
set(gcf,'position',[1         618        1056         384])
p_plotctx(mctxsm, mctxsm.dvar,[minmax(mctxsm.dvar)]);
colormap(varycolor(100,'-viridis'))
arrayfun(@(x)set(x,'position',get(x,'position')+[100,0,0,0]),findobj(gcf,'type','axes'))
h = colorbar('fontsize',20,'units','pixels','position',[96.2000   56.8000   20.7000  280.3000]);
ylabel(h,'Distance (mm)')
set(gcf,'paperpositionmode','auto')
drawnow
print('-dpng','-r300',fullfile(figdir,'measured_dpos.png'))

figure(999);clf
ipos = [12 10]; icol = 4;
histogram(mctxsm.dvar,nbins,histopts{:})
set(gca,'fontsize',fontsize)
yticklabels(yticks(gca)/1000)
xlabel('Distance (mm)','FontSize',fontsize)
ylabel('Number of sources (x 1000)','FontSize',fontsize)
ylim([0 1050])
vline(spacetable{ipos(1),icol}/10,'color',cols(1,:),'linewidth',2)
vline(spacetable{ipos(2),icol}/10,'color',cols(2,:),'linewidth',2)
print('-dpng','-r300',fullfile(figdir,'measured_dpos_hist.png'))


% plot radiality (ori wrt sensor)
figure(35);clf
set(gcf,'position',[1         618        1056         384])
p_plotctx(mctxsm, rad2deg(acos(abs(mctxsm.radiality))),[0 90]);
colormap(varycolor(256,'viridis'))
% colormap(varycolor(256,{'grey','red'}))
arrayfun(@(x)set(x,'position',get(x,'position')+[100,0,0,0]),findobj(gcf,'type','axes'))
h = colorbar('fontsize',20,'units','pixels','position',[96.2000   56.8000   20.7000  280.3000]);
set(h,'Ticks',[0 90],'TickLabels',{'Rad.','Tan.'},'direction','reverse')
ylabel(h,{'Avg. orientation'  'wrt. closest sensor'});
set(gcf,'paperpositionmode','auto')
drawnow
print('-dpng','-r300',fullfile(figdir,'measured_orisens.png'))

% ori wrt sens radiality
figure(999);clf
ipos = [2 9]; icol = 5;
histogram(rad2deg(acos(mctxsm.radiality)),nbins,histopts{:})
set(gca,'fontsize',fontsize)
yticklabels(yticks(gca)/1000)
xlim([0 180])
xtick([0 45 90 135 180]);
xticklabels([0 45 90 135 180]);
xlabel('Average orientation (deg.)','FontSize',fontsize)
ylabel('Number of sources (x 1000)','FontSize',fontsize)
ylim([0 1040])
vline(spacetable{ipos(1),icol},'color',cols(1,:),'linewidth',2)
vline(spacetable{ipos(2),icol},'color',cols(2,:),'linewidth',2)
print('-dpng','-r300',fullfile(figdir,'measured_orisens_hist.png'))

% position variability
figure(31);clf
set(gcf,'position',[1         618        1056         384])
% posvol = 4/3*pi*prod(std(cat(3,allctxsm.pos),[],3)*1000,2);
p_plotctx(mctxsm, mctxsm.posvar,[minmax(mctxsm.posvar)],1);
colormap(varycolor(256,'-viridis'))
arrayfun(@(x)set(x,'position',get(x,'position')+[100,0,0,0]),findobj(gcf,'type','axes'))
h = colorbar('fontsize',20,'units','pixels','position',[96.2000   56.8000   20.7000  280.3000]);
ylabel(h,'Position stdev (mm)');
set(gcf,'paperpositionmode','auto')
drawnow
print('-dpng','-r300',fullfile(figdir,'measured_posvar.png'))

figure(999);clf
ipos = [7 4]; icol = 2;
set(gcf,'position',[ 1360         614         561         384])
histogram(mctxsm.posvar,nbins,histopts{:})
set(gca,'fontsize',fontsize)
yticklabels(yticks(gca)/1000)
xlabel('Position stdev (mm)','FontSize',fontsize)
ylabel('Number of sources (x 1000)','FontSize',fontsize)
drawnow
vline(spacetable{ipos(1),icol},'color',cols(1,:),'linewidth',2)
vline(spacetable{ipos(2),icol},'color',cols(2,:),'linewidth',2)
print('-dpng','-r300',fullfile(figdir,'measured_posvar_hist.png'))

% orientation variability
figure(32);clf
set(gcf,'position',[1         618        1056         384])
p_plotctx(mctxsm,mctxsm.orivar,[0 1]);
arrayfun(@(x)set(x,'position',get(x,'position')+[100,0,0,0]),findobj(gcf,'type','axes'))
h = colorbar('fontsize',20,'units','pixels','position',[96.2000   56.8000   20.7000  280.3000]);
h.Ticks = [0 1];
ylabel(h,{'Resultant vector' 'length (AU)'})
set(gcf,'paperpositionmode','auto')
drawnow
print('-dpng','-r300',fullfile(figdir,'measured_ori.png'))

figure(999);clf
ipos = [6 3]; icol = 3;
histogram(mctxsm.orivar,nbins,histopts{:})
set(gca,'fontsize',fontsize)
yticklabels(yticks(gca)/1000)
xlabel('Resultant vector length (AU)','FontSize',fontsize)
xlim([0 1])
ylabel('Number of sources (x 1000)','FontSize',fontsize)
drawnow
vline(spacetable{ipos(1),icol},'color',cols(1,:),'linewidth',2)
vline(spacetable{ipos(2),icol},'color',cols(2,:),'linewidth',2)
print('-dpng','-r300',fullfile(figdir,'measured_ori_hist.png'))
