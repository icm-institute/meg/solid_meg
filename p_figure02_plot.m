% this script plots the results of figure02_compute
%
% 
p_setpath

fs = p_dbload('restin',{'name','label','ds','atlas164','grad','hm','transform'});
% swap
fs = renamefields(fs,'atlas164','ctxsm');
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end

root_jobsdir = fullfile(jobsdir,'/figure2');

figdir = fullfile(figsdir,'Figure2');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

p_neighb

mctxsm = p_create_meanctx('164',fs);

%% list all jobs run by previous script

alljobs = flister('amp_(?<amp>\d+)/clusterjob(?<ijob>...).mat','dir',root_jobsdir);
choose_amp = 10;
alljobs = flist_select(alljobs,'amp',num2str(choose_amp))';
alljobs = flist_consolidate(alljobs);

%% read results of all jobs.
clear MCs HH
for ibatch =1:size(alljobs,1)
    [MCs{ibatch},HH{ibatch}] = p_gather_clusterjobs(alljobs(ibatch,:));
end

% compute nsujs and ntris from results
outMCs = MCs{1}(:,:,1);
[outMCs.sujs] = rep2struct(cellfun(@numel,{outMCs.sujs}));
[outMCs.tris] = rep2struct(cellfun(@(x)size(x,2),{outMCs.tris}));
nsujs = unique([outMCs.sujs]);
ntris = unique([outMCs.tris]);


%% plot simply the grand average MC power for each source
for ibatch = 1:size(alljobs,1)
    for insujs = 1:size(HH{ibatch},1)
        for intris = 1:size(HH{ibatch},2)
            
            toplot = squeeze(mean(mean(mean(HH{ibatch}(insujs,intris,:,:),1),2),4));
            figure(555+ibatch);clf
            set(gcf,'position',[1         618        1056         384])
            
            p_plotctx(mctxsm,toplot,[0 1],0.5);
            
            colormap(varycolor(256,'Moreland_Black_Body'))
            h = colorbar('fontsize',20,'units','pixels','position',[929.2,56.8,20.7,280.3]);
            ylabel(h,'Power');
            set(gcf,'color',colors('grey80'))%'none');%
            delete(findobj(gcf,'type','light'))
            set(gcf,'paperpositionmode','auto')
            drawnow
%            print('-dpng','-r300',fullfile(figdir,['amp_' alljobs(ibatch).amp '_' num2str(ntris(intris)) 'tris_' num2str(nsujs(insujs)) 'subjs_ctxplot_nolight_grey.png']));
            export_fig(fullfile(figdir,['amp_' alljobs(ibatch).amp '_' num2str(ntris(intris)) 'tris_' num2str(nsujs(insujs)) 'subjs_ctxplot_nolight_grey.png']),'-r300','-nocrop')
            
        end
    end
end

