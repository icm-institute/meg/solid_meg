% just setting path:
% removing any occurence of "fieldtrip" from the path
% and adding the correct one

rootdir = '/network/lustre/iss01/cenir/analyse/meeg/00_max/SOLID_MEEG/replicate/';
codedir = fileparts(mfilename('fullpath'));
datadir = fullfile(rootdir,'HCPunzipped');
jobsdir = fullfile(rootdir,'jobs');
figsdir = fullfile(rootdir,'Figures');

% for cleaner output
global ft_default
ft_default.trackcallinfo = 'no';
ft_default.showcallinfo = 'no';
clear ft_default

% add my misc functions
%
addpath(fullfile(codedir,'miscMatlab')) % todo: point to github release
addpath(fullfile(codedir,'miscMatlab/stats'))
addpath(fullfile(codedir,'miscMatlab/plot'))
addpath(fullfile(codedir,'miscMatlab/plot/panel'))
addpath(fullfile(codedir,'miscMatlab/plot/export_fig'))
addpath(fullfile(codedir,'miscMatlab/CircStat2012a'))

mymkdir({datadir,jobsdir,figsdir})

rm_frompath /fieldtrip
% some functions in r10442 are broken with matlab2017 (nargin not supported in scripts)
% addpath(fullfile(cd,'fieldtrip-r10442'));
 

% in case I used eeglab recently, avoid conflicts
rm_frompath('eeglab.*Fieldtrip')

% use fieldtrip located in codedir
addpath(fullfile(codedir, 'fieldtrip'));

ft_defaults




