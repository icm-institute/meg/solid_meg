function [fs] = p_figure06_10_14_18_source_comp(fs,isuj,sm_specs,initial)



% if we measure chan position by distance to closest sensor
if not(isnan(sm_specs.chand))
    % take position of closest sensor, switch to spherical coords,
    % add chand to radius, switch back to cartesian.
    tmp = fs(isuj).grad.chanpos(chnb(initial.closest,fs(isuj).grad.label),:);
    [tmp(1),tmp(2),tmp(3)] = cart2sph(tmp(1),tmp(2),tmp(3));
    tmp(3) = tmp(3) - sm_specs.chand;
    [tmp(1),tmp(2),tmp(3)] = sph2cart(tmp(1),tmp(2),tmp(3));
    fs(isuj).sm.pos = tmp;
else
    % otherwise we just start with prespecified position.
    fs(isuj).sm.pos = sm_specs.pos;
end
% if we measure chan orientation wrt closest sensor
if not(all(isnan(sm_specs.chanmom)))
    % start with orientation of closest sensor
    % find closest sensor:
    [ichan(isuj)] = dsearchn(fs(isuj).grad.chanpos,fs(isuj).sm.pos);
    [~, ichanname(isuj)] = chnb(ichan(isuj),fs(isuj).grad.label);
    % its orientation
    tmp = fs(isuj).grad.chanori(ichan(isuj),:);
    % rotate phi radians around N axis
    phi = deg2rad(sm_specs.chanmom(1));
    U = [tmp]';
    N = [0 1 0]';
    v = cos(phi) * U +(1-cos(phi))*dot(U,N)*N + sin(phi) * cross(N,U);
    % save mom
    fs(isuj).sm.mom = v;
else
    % otherwise just take prespecified
    fs(isuj).sm.mom = sm_specs.mom';
end

% sampling positions normally in 3d
tmp = randn(1,3) .* sm_specs.posvar;
fs(isuj).sm.pos = fs(isuj).sm.pos + tmp;

% sampling orientations normally
% switch to spherical coords
[tmp(1),tmp(2),tmp(3)] = cart2sph(fs(isuj).sm.mom(1),fs(isuj).sm.mom(2),fs(isuj).sm.mom(3));
% add random orientation to azimuth & elevation
tmp(1:2) = tmp(1:2) + randn(1,2) .* deg2rad(sm_specs.momvar/2);

% add random amplitude change
tmp(3) = tmp(3) + (rand .* 2 -1) .* sm_specs.ampvar;
[fs(isuj).sm.mom(1),fs(isuj).sm.mom(2),fs(isuj).sm.mom(3)]  = sph2cart(tmp(1),tmp(2),tmp(3));

% compute leadfield for this source
fs(isuj).lf = ft_compute_leadfield(fs(isuj).sm.pos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
