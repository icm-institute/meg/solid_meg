% rewrite trial data for faster access
% extract data from the Restin_rmegpreproc.mat files, interpolate missing
% channels and order them consistently.
% at the end of this, all subjects should have an alltrials.mat file with
% all trials to be loaded individually

p_setpath

rewritetrials = 1;
if rewritetrials
    
    clear
    
    overwritetrials = 0;
    
    rootdir_Restin = '~/analyse_max/SOLID_MEEG/replicate/HCPunzipped';
    fs = flister('(?<ds>\d{6})_MEG_(?<run>\d+)-Restin_rmegpreproc.mat','dir',rootdir_Restin);
    fs = flist_consolidate(fs);
    load('bti248_neighb.mat')
    
    for ids = 1:size(fs,1)
        disp(['Processing ' fs(ids,1).ds ' ('  num2str(ids) '/' num2str(size(fs,1)) ')'])
        if overwritetrials || ~exist(fullfile(fileparts(fs(ids,1).name),[fs(ids,1).ds '_alltrials.mat']),'file')
            for irun = 1:size(fs,2)
                tmp = load(fs(ids,irun).name);
                d{irun} = tmp.data;
                % prepare neighbours
                
                % interpolate missing channels
                megchans_orig = ft_channelselection('meg',d{irun}.grad.labelorg);
                missingchans = megchans_orig(~ismember(megchans_orig,d{irun}.label));
                cfg = [];
                cfg.missingchannel = missingchans;
                cfg.neighbours = neighbours;
                cfg.method = 'spline';
                d{irun} = ft_channelrepair(cfg,d{irun});
                
                % keep channel order consistent
                nuorder = regexpcell(d{irun}.label,megchans_orig,'exact');
                for i = 1:numel(d{irun}.trial)
                    d{irun}.trial{i} = d{irun}.trial{i}(nuorder,:);
                end
                d{irun}.label = d{irun}.label(nuorder);
            end
            
            % append runs
            data = ft_appenddata([],d{:});
            if not(isequal(data.label,megchans_orig))
                error('mixed up channels?');
            end
            
            % copy all data to trial### variables for selective loading
            clear trial*
            ntrials = numel(data.trial);
            for i = 1:ntrials
                eval(sprintf('trial%03d=data.trial{%d};',i,i));
            end
            % remove trials from data variable
            data = rmfield(data,'trial');
            data.time = data.time(1);
            data.grad = d{1}.grad;
            data.hdr = d{1}.hdr;
            data.fsample = d{1}.fsample;
            
            % save to disk
            disp(['saving ... ' num2str(ids) '/' num2str(size(fs,1))])
            if exist(fullfile(fileparts(fs(ids,1).name),'alltrials.mat'),'file')
                i = 1;
                while exist(fullfile(fileparts(fs(ids,1).name),sprintf('old_alltrials%d.mat',i)),'file')
                    i = i+1;
                end
                movefile(fullfile(fileparts(fs(ids,1).name),'alltrials.mat'),fullfile(fileparts(fs(ids,1).name),sprintf('old_alltrials%d.mat',i)))
            end
            save(fullfile(fileparts(fs(ids,1).name),[fs(ids,1).ds '_alltrials.mat']),'data','trial*','ntrials')
        end
    end
    
end