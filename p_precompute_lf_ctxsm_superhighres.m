%% this script precomputes the leadfield for the superhigh resolution source model
% it can be run on several matlab instances in parallel (>300 000 lfs to compute)

p_setpath

fs = p_dbload('restin',{'name','label','ds','atlas164','grad','hm','transform'});

fs = renamefields(fs,'atlas164','ctxsm');
for i = 1:size(fs,1)
    fs(i,1).ctxsm.nrm = normals(fs(i,1).ctxsm.pos, fs(i,1).ctxsm.tri);
end


for i_s = 1:size(fs(1).ctxsm.pos,1)
    %         tic
    %% compute leadfield
    f = fullfile(datadir,'ctxsm_all_lf_superhighres/Native_164k_fs_LR',num2str(floor(i_s/1000),'%03d'),['lf_' num2str(i_s,'%06d') '.mat']);
    mymkdir(fileparts(f))
    
    disp(i_s)
    if exist(f,'file')
        try
            load(f)
            continue
        catch
            delete(f)
        end
    end
    if ~exist(f,'file')
        fid = fopen(f,'w');
        fclose(fid);
        for isuj = 1:size(fs,1)
            
            fs(isuj).dippos = fs(isuj).ctxsm.pos(i_s,:);
            fs(isuj).dipmom = fs(isuj).ctxsm.nrm(i_s,:)';
            fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
            
            fs(isuj).lf = ft_compute_leadfield(fs(isuj).dippos, fs(isuj).grad, fs(isuj).hm,'dipoleunit','nA*m'); % in T/(nA*m)
            fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:) = -fs(isuj).lf(chnb('A2',fs(isuj).grad.label),:);
            fs(isuj).lf = fs(isuj).lf(chnb(fs(isuj).label,fs(isuj).grad.label),:);
            
        end
        
        lf = cat(3,fs(:,1).lf);
        save(f,'lf');
        clear lf
    end
end
