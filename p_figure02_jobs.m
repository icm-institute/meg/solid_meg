function p_figure02_jobs(ijob)

% for all sources of the the superhigh resolution mesh, take nsubj, ntri,
% compute a simple, inject signal, simple t-test, store result value

% preparing environment
codedir = '/network/lustre/iss01/cenir/analyse/meeg/00_max/SOLID_MEEG/replicate/simuHCP/';
addpath(codedir)
p_setpath

load('../../common.mat','allctxsm','alldata','allgrad');
load('../clusterjob_common.mat','fs','specif')
[fs.ctxsm] = rep2struct(allctxsm);
[fs.grad] = rep2struct(allgrad);

struct2ws(specif)

load(sprintf('../clusterjob%03d.mat',ijob),'MCs')

H = false([size(MCs) size(fs(1).ctxsm.pos,1)]);
%% start looping through sources
for i_s = 1:size(fs(1).ctxsm.pos,1)
    %         tic
    if isfield(fs,'lf')
        fs = rmfield(fs,'lf');
    end
    %% load precomputed leadfield 
    load(fullfile(datadir,'ctxsm_all_lf_superhighres/','Native_164k_fs_LR',num2str(floor(i_s/1000),'%03d'),['lf_' num2str(i_s,'%06d')]),'lf');
    % now for each subject
    % we stack leadfields in the fs structure
    for isuj = 1:size(fs,1)
        % mark dipole position
        fs(isuj).dippos = fs(isuj).ctxsm.pos(i_s,:);
        fs(isuj).dipmom = fs(isuj).ctxsm.nrm(i_s,:)';
        fs(isuj).dipmom = fs(isuj).dipmom ./ norm(fs(isuj).dipmom);
        % take leadfield
        fs(isuj).lf = lf(:,:,isuj);

        if 0
            %% control plots
            %% individual source, dipole, and projection
            lftopo = fs(isuj).lf * fs(isuj).dipmom * amp;
            %             lftopo =  lftopo+alldata(:,1,2,1);
            tmp = [];
            tmp.avg = lftopo;

            tmp.time = 1;
            tmp.label = fs(isuj).label;
            tmp.grad = fs(isuj).grad;
            tmp.dimord = 'chan_time';

            % projection to sensors
            figure(6868);clf;
            cfg = [];
            cfg.layout = '4D248.lay';
            cfg.zlim = 'absmax'; [-3.76 3.76]*1e-13;
            %             cfg.zlim = [-1e-13 1e-13];
            cfg.interactive = 'no';
            cfg.comment = 'no';
            cfg.markersize = 10;
            cfg.gridscale = 200;
            ft_topoplotER(cfg,tmp);
            colorbar

            % head and source model
            figure(3390);clf
            ft_plot_vol(fs(isuj).hm,'vertexcolor','none','facecolor','skin','edgecolor','none','facealpha',.5);
            hold on
            % source model (cortex)
            ft_plot_mesh(fs(isuj).ctxsm)
            tmp = fs(isuj).ctxsm;
            tmpgrad = fs(1).grad;
            [sel,meglabels] = chnb(fs(1).label,tmpgrad.label);
            tmpgrad.label = tmpgrad.label(sel);
            tmpgrad.chanori = tmpgrad.chanori(sel,:);
            tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
            tmpgrad.chantype = tmpgrad.chantype(sel);
            tmpgrad.chanunit = tmpgrad.chanunit(sel);
            tmpgrad.tra = tmpgrad.tra(sel,:);
            ft_plot_sens(tmpgrad)

            quiver3(fs(isuj).dippos(1),fs(isuj).dippos(2),fs(isuj).dippos(3),fs(isuj).dipmom(1)/10,fs(isuj).dipmom(2)/10,fs(isuj).dipmom(3)/10,'r','linewidth',5,'maxheadsize',2)
            lighting gouraud
            material shiny
            camlight
            %             view(140,20)
            rotate3d on
            drawnow
        end
    end
    if 0
        %% dipole projection
        for isuj = 1:size(fs,1)
            lftopo(:,:,isuj) = fs(isuj).lf * fs(isuj).dipmom * amp;
        end
        lftopo = mean(lftopo,3);
        %             lftopo =  lftopo+alldata(:,1,2,1);
        tmp = [];
        tmp.avg = lftopo;

        tmp.time = 1;
        tmp.label = fs(isuj).label;
        tmp.grad = fs(isuj).grad;
        tmp.dimord = 'chan_time';

        % projection to sensors
        figure(6868+i_spec);clf;
        cfg = [];
        cfg.layout = '4D248.lay';
        cfg.zlim = 'absmax'; [-3.76 3.76]*1e-13;
        %             cfg.zlim = [-1e-13 1e-13];
        cfg.interactive = 'no';
        cfg.comment = 'no';
        cfg.markersize = 10;
        cfg.gridscale = 200;
        ft_topoplotER(cfg,tmp);
        colorbar
        %% individual source, dipole

        % head and source model
        figure(3390+i_spec);clf
        ft_plot_vol(fs(isuj).hm,'vertexcolor','none','facecolor','skin','edgecolor','none','facealpha',.5);
        hold on
        % source model (cortex)
        ft_plot_mesh(fs(isuj).ctxsm)
        tmp = fs(isuj).ctxsm;
        tmpgrad = fs(1).grad;
        [sel,meglabels] = chnb(fs(1).label,tmpgrad.label);
        tmpgrad.label = tmpgrad.label(sel);
        tmpgrad.chanori = tmpgrad.chanori(sel,:);
        tmpgrad.chanpos = tmpgrad.chanpos(sel,:);
        tmpgrad.chantype = tmpgrad.chantype(sel);
        tmpgrad.chanunit = tmpgrad.chanunit(sel);
        tmpgrad.tra = tmpgrad.tra(sel,:);
        ft_plot_sens(tmpgrad)

        dippos = vertcat(fs.dippos);
        dipmom = horzcat(fs.dipmom)';
        quiver3(dippos(:,1),dippos(:,2),dippos(:,3),dipmom(:,1)/10,dipmom(:,2)/10,dipmom(:,3)/10,'r','linewidth',2,'maxheadsize',2,'autoscale','off')
        lighting gouraud
        material shiny
        camlight
        %             view(140,20)
        rotate3d on
        drawnow
    end
    
    nchan = size(alldata,1);
    ntimes = numel(specif.times);%size(alldata,2);
    fprintf('source%06d\n',i_s)
    % now for all of the nMC data resamples
    for iMC = 1:numel(MCs)
        % find maxsensor across subjects of this MC
        clear alldipdat
        for i = 1:numel(MCs(iMC).sujs)
            alldipdat(:,:,i) = fs(MCs(iMC).sujs(i)).lf * fs(MCs(iMC).sujs(i)).dipmom * amp;
        end
        [~,maxsensor] = maxabs(mean(alldipdat,3));
        % dimensions:
        % sensors, time, subjects;
%         tic
        d1 = NaN([nchan,ntimes,numel(MCs(iMC).sujs)]);
        d2 = NaN(size(d1));
        % fill the data
        for isuj = 1:numel(MCs(iMC).sujs)

            itri = 1:size(MCs(iMC).tris,2)/2;% first half of trials
            tmp = alldata(:,MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            tmp = nanmean(tmp,3); % avg across trials
            d1(:,:,isuj) = tmp;

            itri = (size(MCs(iMC).tris,2)/2+1):size(MCs(iMC).tris,2);% second half of trials
            tmp = alldata(:,MCs(iMC).times,MCs(iMC).tris(isuj,itri),MCs(iMC).sujs(isuj));
            tmp = nanmean(tmp,3); % avg across trials
            d2(:,:,isuj) = tmp;
            
            %average time
            d1 = mean(d1,2);d2=mean(d2,2);
            
            d2(:,:,isuj) = d2(:,:,isuj) + alldipdat(:,:,isuj);
            if 0
                %%
                tmp = [];
                tmp.avg = alldipdat(:,:,isuj);
                tmp.time = 1;
                tmp.label = fs(isuj).grad.label(chnb('meg',fs(isuj).grad.label));
                tmp.grad = fs(isuj).grad;
                tmp.dimord = 'chan_time';

                figure(6868);clf;
                cfg = [];
                cfg.layout = '4D248.lay';
                cfg.zlim = 'maxabs';
                ft_topoplotER(cfg,tmp);
                drawnow
            end
        end
        % compute t-test
        totest = d1-d2;
        [h,p,~,stat] = ttest(totest,0,'Dim',3);
        % store values
        MCs(iMC).m = mean(totest,3);
        MCs(iMC).h = h(maxsensor);
%         MCs(iMC).hany = any(h);
%         MCs(iMC).maxsensor = maxsensor;
        MCs(iMC).p = single(p);
        MCs(iMC).tstat = single(stat.tstat);
        MCs(iMC).df = stat.df(1);
        MCs(iMC).sd = single(stat.sd);

%         toc
    end
    % saving h to a different variable because we now have the whole ctx
    H(:,:,i_s) = reshape([MCs.h],size(MCs));
%     toc
end

MCs = rmfield(MCs,{'m','h','p','tstat','df','sd'});

% save data in a .mat file to be read by the next plotting script
save(sprintf('../clusterjob%03d.mat',ijob),'MCs','H')
