% figures 6 10 14 18 share the same layout.
% this scripts creates them all
p_setpath

fs = p_dbload('restin',{'name','label','ds','grad','hm','transform'});

p_neighb

root_jobsdir = fullfile(jobsdir,'figure6');
figdir = fullfile(figsdir,'Figure6');mymkdir(figdir)

load(fullfile(datadir,'alldata_50timepoints.mat'))

%%

% number of resamples
nMC = 500;

% benchmark of subjects and trials
nsujs = 5:5:50;
nnsujs = numel(nsujs);
ntris = [2 20:20:200];    % keep rem(ntris,2) == 0
nntris = numel(ntris);

dipamp = 5;

p_all_areas
i_ar = 9;
seed = all_areas.seeds(i_ar);

mctxsm = p_create_meanctx('32',fs);

[sm_specs,initial] = p_figure06_10_14_18_source_specs(mctxsm);


%% collect responses

disp(root_jobsdir)
perf = cell(numel(sm_specs),1);
for ispec = 1:numel(sm_specs)
    % gather results
    [MCs] = p_gather_clusterjobs(fullfile(root_jobsdir, [num2str(ispec,'%02d') '_' sm_specs(ispec).name]));
    
    booboots = struct2table(MCs(:));
    booboots.sujs = cellfun(@numel,booboots.sujs);
    booboots.tris = cellfun(@(x) size(x,2),booboots.tris);
    booboots.name = repmat(sm_specs(ispec).name,size(booboots,1),1);
    booboots.amp = repmat(dipamp,size(booboots,1),1);
    booboots.ispec = repmat(ispec,size(booboots,1),1);
    
    % remove useless variables
    vs = booboots.Properties.VariableNames;
    v = vs(regexpcell(vs,{'tstat','df','sd','m','p','m','times'},'exact'));
    booboots(:,v) = [];
    
    posvar = sm_specs(ispec).posvar;
    orivar = sm_specs(ispec).momvar;
    dsens = mctxsm.dvar(seed);
    orisens = rad2deg(acos(mctxsm.radiality(seed)));
    switch sm_specs(ispec).name(1:5)
        case {'novar' 'posva' 'oriva'}
        case 'dsens'
            dsens = sm_specs(ispec).chand + initial.d;
        case 'orise'
            orisens = sm_specs(ispec).chanmom;
    end
    
    booboots.posvar = repmat(posvar,size(booboots,1),1);
    booboots.orivar = repmat(orivar,size(booboots,1),1);
    booboots.dsens = repmat(dsens,size(booboots,1),1);
    booboots.orisens = repmat(orisens,size(booboots,1),1);
    
    % write to csv
    writetable(booboots,fullfile(root_jobsdir, ['fig6_' num2str(ispec,'%02d') '_' sm_specs(ispec).name '.csv']));

    sm_specs(ispec).perf = grpstats(booboots,{'sujs','tris','name'},'sum','DataVars','h');
    sm_specs(ispec).perf.p = sm_specs(ispec).perf.sum_h ./ sm_specs(ispec).perf.GroupCount;

end

%% power plot

props = {'posvar'};%'dsens','orisens', ,'orivar'};
for iprop = 1:numel(props)
    %%
    tmp_sm_specs = sm_specs(regexpcell({sm_specs.name},props{iprop}));
    fig(15,3838+iprop);
    clf
    for ispec = 1:numel(tmp_sm_specs)
        subplot(1,numel(tmp_sm_specs),ispec)
        set(gca,'FontSize',14);
        colormap(gca,varycolor(256,'Moreland_Black_Body'))
        hold on
        f = flister(['fig4_predperm_.._' tmp_sm_specs(ispec).name '.csv'],'dir',root_jobsdir,'recurse',0);
        perf=tmp_sm_specs(ispec).perf;
        toplot = reshape(perf.p,nntris,nnsujs);
        imagesc(nsujs,ntris,toplot,[0 1])
        levs = [.1 .5 .8];
        [xq,yq] = meshgrid(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)));
        nutoplot = interp2(nsujs,ntris,toplot,xq,yq,'spline');
        contour(linspace(min(nsujs),max(nsujs)),linspace(min(ntris),max(ntris)),nutoplot,levs,'k','showtext','on')
        set(gca,'layer','top')
        box on
        axis xy
        
        xticks(nsujs)
        xticklabels(nsujs);
        set(gca,'XTickLabelRotation',90)
        yticks(ntris)
        yticklabels(num2str(cellstr2num(get(gca,'yticklabel'))/2));
        if ispec == 1
            
            xlabel('Number of subjects')
            ylabel('Number of trials')
        end
        set(gca,'tag','power_contour')
        
        if ispec == numel(tmp_sm_specs)
            h = colorbar('Position',[0.9291    0.1841    0.0152    0.7292]);
            h.Label.String = 'Power';
            h.Label.FontSize =16;
        end
        
    end
    %%
    set(gcf,'paperpositionmode','auto')
    drawnow
    
    print('-dpng','-r300',fullfile(figdir,['PowerPlot_' props{iprop} '.png']))
    
end



