function mctxsm = p_create_meanctx(res,fs)

switch res
    case '164'
        f = 'mctxsm164.mat';
        npos = 327684;
    case '32'
        f = 'mctxsm.mat';
        npos = 64984;
end
    
% this script loads / creates a mean cortical mesh

if exist(f,'file')
    load(f)
else
    for fi = {'sulc','curv','thickness','atlasroi'}
        try
            fs(1).ctxsm = rmfield(fs(1).ctxsm,fi);
        end
    end
    mctxsm = fs(1).ctxsm;
    if size(mctxsm.pos,1) ~= npos
        error('wrong number of vertices')
    end
    allctxsm = [fs(:,1).ctxsm];
    mctxsm.pos = mean(cat(3,allctxsm.pos),3);
    mctxsm.allpos = cat(3,allctxsm.pos);
    mctxsm.alltri = cat(3,allctxsm.tri);
    mctxsm.sdpos = mean(std(cat(3,allctxsm.pos),[],3),2);
    mctxsm.mnrm = mean(cat(3,allctxsm.nrm),3);
    mctxsm.nrm = normals(mctxsm.pos,mctxsm.tri);
    mctxsm.allnrm = cat(3,allctxsm.nrm);
    % position variability
    mctxsm.posvar = mctxsm.sdpos*1000;
    %orientation variability
    mctxsm.orivar =  sqrt(sum(mctxsm.mnrm.^2,2));
    % distance to sensors
    dvar = NaN(size(mctxsm.pos,1),1);
    for i_s = 1:size(mctxsm.pos,1)
        d = NaN(size(fs,1),1);
        for isuj = 1:size(fs,1)
            [~,d(isuj)] = dsearchn(fs(isuj).grad.chanpos,fs(isuj).ctxsm.pos(i_s,:));
        end
        dvar(i_s) = mean(d)*100;
    end
    mctxsm.dvar = dvar;
    % orientation wrt sensors
    o = NaN(size(mctxsm.pos,1),size(fs,1));
    for i_s = 1:size(mctxsm.pos,1)
        for i_f = 1:size(fs,1)
            % find closest sensor
            [ichan] = dsearchn(fs(i_f).grad.chanpos,fs(i_f).ctxsm.pos(i_s,:));
            % compute orientation relative to that sensor
            % keep value
            o(i_s,i_f) = fs(i_f).ctxsm.nrm(i_s,:) * fs(i_f).grad.chanori(ichan,:)';
        end
    end
    mctxsm.radiality = mean(o,2);
    so = std(o,[],2);
    save(f,'mctxsm')
end